module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./lib/auth.js":
/*!*********************!*\
  !*** ./lib/auth.js ***!
  \*********************/
/*! exports provided: AuthProvider, useAuth */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthProvider", function() { return AuthProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useAuth", function() { return useAuth; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! js-cookie */ "js-cookie");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _lib_db__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @lib/db */ "./lib/db.js");
/* harmony import */ var _lib_firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @lib/firebase */ "./lib/firebase.js");

var _jsxFileName = "D:\\Development\\Full-stack\\vlamer\\lib\\auth.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }





const authContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createContext"])();
function AuthProvider({
  children
}) {
  const auth = useProvideAuth();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(authContext.Provider, {
    value: auth,
    children: children
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 11,
    columnNumber: 10
  }, this);
}
const useAuth = () => {
  return Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(authContext);
};

function useProvideAuth() {
  const {
    0: user,
    1: setUser
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);

  const handleUser = async rawUser => {
    console.log(rawUser);

    if (rawUser) {
      const _formatUser = formatUser(rawUser),
            {
        token
      } = _formatUser,
            userDataWithoutToken = _objectWithoutProperties(_formatUser, ["token"]);

      js_cookie__WEBPACK_IMPORTED_MODULE_2___default.a.set("vlamer-auth", true, {
        expires: 1
      });
      await Object(_lib_db__WEBPACK_IMPORTED_MODULE_3__["createUser"])(userDataWithoutToken.uid, userDataWithoutToken);
      setUser(_objectSpread(_objectSpread({}, userDataWithoutToken), {}, {
        token: token
      }));
      return user;
    } else {
      js_cookie__WEBPACK_IMPORTED_MODULE_2___default.a.remove("vlamer-auth");
      setUser(false);
      return false;
    }
  };

  const signinWithGithub = async (email, password) => {
    return await _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().signInWithPopup(new _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth.GithubAuthProvider()).then(response => {
      handleUser(response.user);
    });
  };

  const signinWithGoogle = async (email, password) => {
    return await _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().signInWithPopup(new _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth.GoogleAuthProvider()).then(response => {
      handleUser(response.user);
    });
  };

  const signout = async () => {
    return await _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().signOut().then(() => {
      handleUser(false);
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    const unsubscribe = _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().onAuthStateChanged(handleUser);
    return () => unsubscribe();
  }, []);
  return {
    user,
    signinWithGithub,
    signinWithGoogle,
    signout
  };
}

const formatUser = user => {
  return {
    uid: user.uid,
    email: user.email,
    name: user.displayName,
    username: user.displayName.substr(0, 2) + user.uid.substr(0, 4),
    token: user.ya,
    provider: user.providerData[0].providerId,
    imgUrl: user.providerData[0].photoURL
  };
};

/***/ }),

/***/ "./lib/db.js":
/*!*******************!*\
  !*** ./lib/db.js ***!
  \*******************/
/*! exports provided: createUser, getAllUsers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createUser", function() { return createUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllUsers", function() { return getAllUsers; });
/* harmony import */ var _lib_firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @lib/firebase */ "./lib/firebase.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const firestore = _lib_firebase__WEBPACK_IMPORTED_MODULE_0__["default"].firestore();
const createUser = async (uid, data) => {
  return firestore.collection("users").doc(uid).set(_objectSpread({}, data), {
    merge: true
  });
};
const getAllUsers = async () => {
  const snapshot = await firestore.collection("users").get();
  let response = {};
  snapshot.forEach(doc => {
    response[doc.id] = _objectSpread({}, doc.data());
  });
  console.log(response);
  return response;
};

/***/ }),

/***/ "./lib/firebase.js":
/*!*************************!*\
  !*** ./lib/firebase.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/app */ "firebase/app");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/auth */ "firebase/auth");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_auth__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/firestore */ "firebase/firestore");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_firestore__WEBPACK_IMPORTED_MODULE_2__);




if (!firebase_app__WEBPACK_IMPORTED_MODULE_0___default.a.apps.length) {
  firebase_app__WEBPACK_IMPORTED_MODULE_0___default.a.initializeApp({
    apiKey: "AIzaSyB8HWgzpTqOJCK0CixMzhBVuJYTryyRf1c",
    authDomain: "next-js-blog-89df8.firebaseapp.com",
    projectId: "next-js-blog-89df8"
  });
}

/* harmony default export */ __webpack_exports__["default"] = (firebase_app__WEBPACK_IMPORTED_MODULE_0___default.a);

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MyApp; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _styles_Theme__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @styles/Theme */ "./styles/Theme.js");
/* harmony import */ var _lib_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @lib/auth */ "./lib/auth.js");

var _jsxFileName = "D:\\Development\\Full-stack\\vlamer\\pages\\_app.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Core components



 // Components



function MyApp(props) {
  const {
    Component,
    pageProps
  } = props;
  react__WEBPACK_IMPORTED_MODULE_1___default.a.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");

    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Paper"], {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_3___default.a, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        http: "Content-Type",
        content: "text/html; charSet=utf-8"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Abel's resume"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 25,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        content: "This is personal portfolio site to showcase the projects that abel has been a part of. ",
        name: "description"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        content: "width=device-width, initial-scale=1",
        name: "viewport"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        http: "Content-Type",
        content: "text/html; charSet=utf-8"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        content: "width=device-width, initial-scale=1",
        name: "viewport"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        name: "msapplication-TileColor",
        content: "#2d89ef"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        name: "theme-color",
        content: "#ffffff"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/img/favicon/apple-touch-icon.png"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: "/img/favicon/favicon-32x32.png"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: "/img/favicon/favicon-16x16.png"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
        rel: "manifest",
        href: "/img/favicon/site.webmanifest"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
        rel: "mask-icon",
        href: "/img/favicon/safari-pinned-tab.svg",
        color: "#5bbad5"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        name: "msapplication-TileColor",
        content: "#da532c"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        name: "theme-color",
        content: "#ffffff"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["CssBaseline"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["ThemeProvider"], {
      theme: _styles_Theme__WEBPACK_IMPORTED_MODULE_5__["default"],
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_lib_auth__WEBPACK_IMPORTED_MODULE_6__["AuthProvider"], {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Component, _objectSpread({}, pageProps), void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 66,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 22,
    columnNumber: 5
  }, this);
}
MyApp.propTypes = {
  Component: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.elementType.isRequired,
  pageProps: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired
};

/***/ }),

/***/ "./styles/Theme.js":
/*!*************************!*\
  !*** ./styles/Theme.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_0__);

const theme = Object(_material_ui_core__WEBPACK_IMPORTED_MODULE_0__["createMuiTheme"])({
  palette: {
    //   background: {
    //     paper: mode === "light" ? "#FFF" : "#000",
    //     default: mode === "light" ? "#FFF" : "#000",
    //     level2: mode === "light" ? "#F5f5f5" : "#050505",
    //     level1: mode === "light" ? "#FFF" : "#000",
    //   },
    type: "light",
    primary: {
      main: "#2c2925"
    },
    secondary: {
      main: "#73e8ff"
    },
    text: {
      primary: "rgba(0, 0, 0)",
      secondary: "rgba(255, 255, 255)"
    },
    divider: "#73e8ff"
  },
  typography: {
    h1: {
      fontFamily: "Coolvetica",
      fontSize: "3.5rem",
      fontWeight: 600,
      letterSpacing: "0.2rem"
    },
    h2: {
      fontFamily: "Coolvetica",
      fontSize: "3rem",
      fontWeight: 400,
      letterSpacing: "0.1rem"
    },
    h3: {
      fontFamily: "Montserrat",
      fontSize: "2.2rem",
      fontWeight: 300,
      letterSpacing: "0.1rem"
    },
    h5: {
      fontFamily: "Coolvetica",
      fontSize: "1.5rem",
      fontWeight: 300,
      letterSpacing: "0.1rem"
    },
    h6: {
      fontFamily: "Coolvetica",
      fontSize: "1.2rem",
      fontWeight: 300,
      letterSpacing: "0.1rem"
    },
    body1: {
      fontFamily: "Montserrat",
      fontSize: "1rem",
      letterSpacing: "0.1rem"
    },
    button: {
      fontFamily: "Montserrat",
      fontSize: "0.8rem",
      letterSpacing: "0.1rem",
      textTransform: "capitalize"
    }
  },
  shadows: ["0px 0px 0px 0px rgba(0, 120, 0, 0.3)", "0px 0px 10px -5px rgba(0, 255, 0, 0.3)", "0px 0px 12px -4px rgba(0, 120, 0, 0.3)", "0px 2px 14px -3px rgba(0, 120, 0, 0.3)", "0px 2px 16px -5px rgba(0, 120, 0, 0.3)", "0px 4px 18px -4px rgba(0, 255, 0, 0.3)", "0px 4px 20px -3px rgba(0, 120, 0, 0.3)", "0px 6px 22px -5px rgba(0, 120, 0, 0.3)", "0px 6px 24px -4px rgba(0, 120, 0, 0.3)", "0px 8px 26px -3px rgba(0, 120, 0, 0.3)", "0px 8px 28px -5px rgba(0, 120, 0, 0.3)", "0px 10px 30px -4px rgba(0, 120, 0, 0.3)", "0px 10px 32px -3px rgba(0, 120, 0, 0.3)"]
});
/* harmony default export */ __webpack_exports__["default"] = (theme);

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi private-next-pages/_app.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.js */"./pages/_app.js");


/***/ }),

/***/ "@material-ui/core":
/*!************************************!*\
  !*** external "@material-ui/core" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core");

/***/ }),

/***/ "firebase/app":
/*!*******************************!*\
  !*** external "firebase/app" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/app");

/***/ }),

/***/ "firebase/auth":
/*!********************************!*\
  !*** external "firebase/auth" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/auth");

/***/ }),

/***/ "firebase/firestore":
/*!*************************************!*\
  !*** external "firebase/firestore" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/firestore");

/***/ }),

/***/ "js-cookie":
/*!****************************!*\
  !*** external "js-cookie" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("js-cookie");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbGliL2F1dGguanMiLCJ3ZWJwYWNrOi8vLy4vbGliL2RiLmpzIiwid2VicGFjazovLy8uL2xpYi9maXJlYmFzZS5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9fYXBwLmpzIiwid2VicGFjazovLy8uL3N0eWxlcy9UaGVtZS5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcImZpcmViYXNlL2FwcFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcImZpcmViYXNlL2F1dGhcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJmaXJlYmFzZS9maXJlc3RvcmVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJqcy1jb29raWVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0L2hlYWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJwcm9wLXR5cGVzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3RcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiJdLCJuYW1lcyI6WyJhdXRoQ29udGV4dCIsImNyZWF0ZUNvbnRleHQiLCJBdXRoUHJvdmlkZXIiLCJjaGlsZHJlbiIsImF1dGgiLCJ1c2VQcm92aWRlQXV0aCIsInVzZUF1dGgiLCJ1c2VDb250ZXh0IiwidXNlciIsInNldFVzZXIiLCJ1c2VTdGF0ZSIsImhhbmRsZVVzZXIiLCJyYXdVc2VyIiwiY29uc29sZSIsImxvZyIsImZvcm1hdFVzZXIiLCJ0b2tlbiIsInVzZXJEYXRhV2l0aG91dFRva2VuIiwiY29va2llIiwic2V0IiwiZXhwaXJlcyIsImNyZWF0ZVVzZXIiLCJ1aWQiLCJyZW1vdmUiLCJzaWduaW5XaXRoR2l0aHViIiwiZW1haWwiLCJwYXNzd29yZCIsImZpcmViYXNlIiwic2lnbkluV2l0aFBvcHVwIiwiR2l0aHViQXV0aFByb3ZpZGVyIiwidGhlbiIsInJlc3BvbnNlIiwic2lnbmluV2l0aEdvb2dsZSIsIkdvb2dsZUF1dGhQcm92aWRlciIsInNpZ25vdXQiLCJzaWduT3V0IiwidXNlRWZmZWN0IiwidW5zdWJzY3JpYmUiLCJvbkF1dGhTdGF0ZUNoYW5nZWQiLCJuYW1lIiwiZGlzcGxheU5hbWUiLCJ1c2VybmFtZSIsInN1YnN0ciIsInlhIiwicHJvdmlkZXIiLCJwcm92aWRlckRhdGEiLCJwcm92aWRlcklkIiwiaW1nVXJsIiwicGhvdG9VUkwiLCJmaXJlc3RvcmUiLCJkYXRhIiwiY29sbGVjdGlvbiIsImRvYyIsIm1lcmdlIiwiZ2V0QWxsVXNlcnMiLCJzbmFwc2hvdCIsImdldCIsImZvckVhY2giLCJpZCIsImFwcHMiLCJsZW5ndGgiLCJpbml0aWFsaXplQXBwIiwiYXBpS2V5IiwicHJvY2VzcyIsImF1dGhEb21haW4iLCJwcm9qZWN0SWQiLCJORVhUX1BVQkxJQ19GSVJFQkFTRV9QUk9KRUNUX0lEIiwiTXlBcHAiLCJwcm9wcyIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyIsIlJlYWN0IiwianNzU3R5bGVzIiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwicGFyZW50RWxlbWVudCIsInJlbW92ZUNoaWxkIiwidGhlbWUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJlbGVtZW50VHlwZSIsImlzUmVxdWlyZWQiLCJvYmplY3QiLCJjcmVhdGVNdWlUaGVtZSIsInBhbGV0dGUiLCJ0eXBlIiwicHJpbWFyeSIsIm1haW4iLCJzZWNvbmRhcnkiLCJ0ZXh0IiwiZGl2aWRlciIsInR5cG9ncmFwaHkiLCJoMSIsImZvbnRGYW1pbHkiLCJmb250U2l6ZSIsImZvbnRXZWlnaHQiLCJsZXR0ZXJTcGFjaW5nIiwiaDIiLCJoMyIsImg1IiwiaDYiLCJib2R5MSIsImJ1dHRvbiIsInRleHRUcmFuc2Zvcm0iLCJzaGFkb3dzIl0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsSUFBSTtRQUNKO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEZBO0FBQ0E7QUFFQTtBQUNBO0FBRUEsTUFBTUEsV0FBVyxnQkFBR0MsMkRBQWEsRUFBakM7QUFFTyxTQUFTQyxZQUFULENBQXNCO0FBQUVDO0FBQUYsQ0FBdEIsRUFBb0M7QUFDekMsUUFBTUMsSUFBSSxHQUFHQyxjQUFjLEVBQTNCO0FBQ0Esc0JBQU8scUVBQUMsV0FBRCxDQUFhLFFBQWI7QUFBc0IsU0FBSyxFQUFFRCxJQUE3QjtBQUFBLGNBQW9DRDtBQUFwQztBQUFBO0FBQUE7QUFBQTtBQUFBLFVBQVA7QUFDRDtBQUVNLE1BQU1HLE9BQU8sR0FBRyxNQUFNO0FBQzNCLFNBQU9DLHdEQUFVLENBQUNQLFdBQUQsQ0FBakI7QUFDRCxDQUZNOztBQUlQLFNBQVNLLGNBQVQsR0FBMEI7QUFDeEIsUUFBTTtBQUFBLE9BQUNHLElBQUQ7QUFBQSxPQUFPQztBQUFQLE1BQWtCQyxzREFBUSxDQUFDLElBQUQsQ0FBaEM7O0FBRUEsUUFBTUMsVUFBVSxHQUFHLE1BQU9DLE9BQVAsSUFBbUI7QUFDcENDLFdBQU8sQ0FBQ0MsR0FBUixDQUFZRixPQUFaOztBQUNBLFFBQUlBLE9BQUosRUFBYTtBQUNYLDBCQUEyQ0csVUFBVSxDQUFDSCxPQUFELENBQXJEO0FBQUEsWUFBTTtBQUFFSTtBQUFGLE9BQU47QUFBQSxZQUFrQkMsb0JBQWxCOztBQUNBQyxzREFBTSxDQUFDQyxHQUFQLENBQVcsYUFBWCxFQUEwQixJQUExQixFQUFnQztBQUFFQyxlQUFPLEVBQUU7QUFBWCxPQUFoQztBQUNBLFlBQU1DLDBEQUFVLENBQUNKLG9CQUFvQixDQUFDSyxHQUF0QixFQUEyQkwsb0JBQTNCLENBQWhCO0FBQ0FSLGFBQU8saUNBQU1RLG9CQUFOO0FBQTRCRCxhQUFLLEVBQUVBO0FBQW5DLFNBQVA7QUFFQSxhQUFPUixJQUFQO0FBQ0QsS0FQRCxNQU9PO0FBQ0xVLHNEQUFNLENBQUNLLE1BQVAsQ0FBYyxhQUFkO0FBQ0FkLGFBQU8sQ0FBQyxLQUFELENBQVA7QUFDQSxhQUFPLEtBQVA7QUFDRDtBQUNGLEdBZEQ7O0FBZ0JBLFFBQU1lLGdCQUFnQixHQUFHLE9BQU9DLEtBQVAsRUFBY0MsUUFBZCxLQUEyQjtBQUNsRCxXQUFPLE1BQU1DLHFEQUFRLENBQ2xCdkIsSUFEVSxHQUVWd0IsZUFGVSxDQUVNLElBQUlELHFEQUFRLENBQUN2QixJQUFULENBQWN5QixrQkFBbEIsRUFGTixFQUdWQyxJQUhVLENBR0pDLFFBQUQsSUFBYztBQUNsQnBCLGdCQUFVLENBQUNvQixRQUFRLENBQUN2QixJQUFWLENBQVY7QUFDRCxLQUxVLENBQWI7QUFNRCxHQVBEOztBQVNBLFFBQU13QixnQkFBZ0IsR0FBRyxPQUFPUCxLQUFQLEVBQWNDLFFBQWQsS0FBMkI7QUFDbEQsV0FBTyxNQUFNQyxxREFBUSxDQUNsQnZCLElBRFUsR0FFVndCLGVBRlUsQ0FFTSxJQUFJRCxxREFBUSxDQUFDdkIsSUFBVCxDQUFjNkIsa0JBQWxCLEVBRk4sRUFHVkgsSUFIVSxDQUdKQyxRQUFELElBQWM7QUFDbEJwQixnQkFBVSxDQUFDb0IsUUFBUSxDQUFDdkIsSUFBVixDQUFWO0FBQ0QsS0FMVSxDQUFiO0FBTUQsR0FQRDs7QUFTQSxRQUFNMEIsT0FBTyxHQUFHLFlBQVk7QUFDMUIsV0FBTyxNQUFNUCxxREFBUSxDQUNsQnZCLElBRFUsR0FFVitCLE9BRlUsR0FHVkwsSUFIVSxDQUdMLE1BQU07QUFDVm5CLGdCQUFVLENBQUMsS0FBRCxDQUFWO0FBQ0QsS0FMVSxDQUFiO0FBTUQsR0FQRDs7QUFTQXlCLHlEQUFTLENBQUMsTUFBTTtBQUNkLFVBQU1DLFdBQVcsR0FBR1YscURBQVEsQ0FBQ3ZCLElBQVQsR0FBZ0JrQyxrQkFBaEIsQ0FBbUMzQixVQUFuQyxDQUFwQjtBQUVBLFdBQU8sTUFBTTBCLFdBQVcsRUFBeEI7QUFDRCxHQUpRLEVBSU4sRUFKTSxDQUFUO0FBTUEsU0FBTztBQUNMN0IsUUFESztBQUVMZ0Isb0JBRks7QUFHTFEsb0JBSEs7QUFJTEU7QUFKSyxHQUFQO0FBTUQ7O0FBRUQsTUFBTW5CLFVBQVUsR0FBSVAsSUFBRCxJQUFVO0FBQzNCLFNBQU87QUFDTGMsT0FBRyxFQUFFZCxJQUFJLENBQUNjLEdBREw7QUFFTEcsU0FBSyxFQUFFakIsSUFBSSxDQUFDaUIsS0FGUDtBQUdMYyxRQUFJLEVBQUUvQixJQUFJLENBQUNnQyxXQUhOO0FBSUxDLFlBQVEsRUFBRWpDLElBQUksQ0FBQ2dDLFdBQUwsQ0FBaUJFLE1BQWpCLENBQXdCLENBQXhCLEVBQTJCLENBQTNCLElBQWdDbEMsSUFBSSxDQUFDYyxHQUFMLENBQVNvQixNQUFULENBQWdCLENBQWhCLEVBQW1CLENBQW5CLENBSnJDO0FBS0wxQixTQUFLLEVBQUVSLElBQUksQ0FBQ21DLEVBTFA7QUFNTEMsWUFBUSxFQUFFcEMsSUFBSSxDQUFDcUMsWUFBTCxDQUFrQixDQUFsQixFQUFxQkMsVUFOMUI7QUFPTEMsVUFBTSxFQUFFdkMsSUFBSSxDQUFDcUMsWUFBTCxDQUFrQixDQUFsQixFQUFxQkc7QUFQeEIsR0FBUDtBQVNELENBVkQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdFQTtBQUVBLE1BQU1DLFNBQVMsR0FBR3RCLHFEQUFRLENBQUNzQixTQUFULEVBQWxCO0FBRU8sTUFBTTVCLFVBQVUsR0FBRyxPQUFPQyxHQUFQLEVBQVk0QixJQUFaLEtBQXFCO0FBQzdDLFNBQU9ELFNBQVMsQ0FDYkUsVUFESSxDQUNPLE9BRFAsRUFFSkMsR0FGSSxDQUVBOUIsR0FGQSxFQUdKSCxHQUhJLG1CQUdLK0IsSUFITCxHQUdhO0FBQUVHLFNBQUssRUFBRTtBQUFULEdBSGIsQ0FBUDtBQUlELENBTE07QUFPQSxNQUFNQyxXQUFXLEdBQUcsWUFBWTtBQUNyQyxRQUFNQyxRQUFRLEdBQUcsTUFBTU4sU0FBUyxDQUFDRSxVQUFWLENBQXFCLE9BQXJCLEVBQThCSyxHQUE5QixFQUF2QjtBQUNBLE1BQUl6QixRQUFRLEdBQUcsRUFBZjtBQUNBd0IsVUFBUSxDQUFDRSxPQUFULENBQWtCTCxHQUFELElBQVM7QUFDeEJyQixZQUFRLENBQUNxQixHQUFHLENBQUNNLEVBQUwsQ0FBUixxQkFDS04sR0FBRyxDQUFDRixJQUFKLEVBREw7QUFHRCxHQUpEO0FBS0FyQyxTQUFPLENBQUNDLEdBQVIsQ0FBWWlCLFFBQVo7QUFDQSxTQUFPQSxRQUFQO0FBQ0QsQ0FWTSxDOzs7Ozs7Ozs7Ozs7QUNYUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7QUFFQSxJQUFJLENBQUNKLG1EQUFBLENBQWlCZ0MsSUFBakIsQ0FBc0JDLE1BQTNCLEVBQW1DO0FBQ2pDakMscURBQUEsQ0FBaUJrQyxhQUFqQixDQUErQjtBQUM3QkMsVUFBTSxFQUFFQyx5Q0FEcUI7QUFFN0JDLGNBQVUsRUFBRUQsb0NBRmlCO0FBRzdCRSxhQUFTLEVBQUVGLG9CQUEyQ0c7QUFIekIsR0FBL0I7QUFLRDs7QUFFY3ZDLGtIQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWkE7QUFDQTtBQUNBO0FBQ0E7Q0FHQTs7QUFDQTtBQUNBO0FBQ2UsU0FBU3dDLEtBQVQsQ0FBZUMsS0FBZixFQUFzQjtBQUNuQyxRQUFNO0FBQUVDLGFBQUY7QUFBYUM7QUFBYixNQUEyQkYsS0FBakM7QUFFQUcsOENBQUssQ0FBQ25DLFNBQU4sQ0FBZ0IsTUFBTTtBQUNwQjtBQUNBLFVBQU1vQyxTQUFTLEdBQUdDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixrQkFBdkIsQ0FBbEI7O0FBQ0EsUUFBSUYsU0FBSixFQUFlO0FBQ2JBLGVBQVMsQ0FBQ0csYUFBVixDQUF3QkMsV0FBeEIsQ0FBb0NKLFNBQXBDO0FBQ0Q7QUFDRixHQU5ELEVBTUcsRUFOSDtBQVFBLHNCQUNFLHFFQUFDLHVEQUFEO0FBQUEsNEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSw4QkFDRTtBQUFNLFlBQUksRUFBQyxjQUFYO0FBQTBCLGVBQU8sRUFBQztBQUFsQztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFFRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUZGLGVBR0U7QUFDRSxlQUFPLEVBQUMseUZBRFY7QUFFRSxZQUFJLEVBQUM7QUFGUDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSEYsZUFPRTtBQUFNLGVBQU8sRUFBQyxxQ0FBZDtBQUFvRCxZQUFJLEVBQUM7QUFBekQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVBGLGVBUUU7QUFBTSxZQUFJLEVBQUMsY0FBWDtBQUEwQixlQUFPLEVBQUM7QUFBbEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVJGLGVBU0U7QUFBTSxlQUFPLEVBQUMscUNBQWQ7QUFBb0QsWUFBSSxFQUFDO0FBQXpEO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FURixlQVVFO0FBQU0sWUFBSSxFQUFDLHlCQUFYO0FBQXFDLGVBQU8sRUFBQztBQUE3QztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBVkYsZUFXRTtBQUFNLFlBQUksRUFBQyxhQUFYO0FBQXlCLGVBQU8sRUFBQztBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBWEYsZUFhRTtBQUNFLFdBQUcsRUFBQyxrQkFETjtBQUVFLGFBQUssRUFBQyxTQUZSO0FBR0UsWUFBSSxFQUFDO0FBSFA7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWJGLGVBa0JFO0FBQ0UsV0FBRyxFQUFDLE1BRE47QUFFRSxZQUFJLEVBQUMsV0FGUDtBQUdFLGFBQUssRUFBQyxPQUhSO0FBSUUsWUFBSSxFQUFDO0FBSlA7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWxCRixlQXdCRTtBQUNFLFdBQUcsRUFBQyxNQUROO0FBRUUsWUFBSSxFQUFDLFdBRlA7QUFHRSxhQUFLLEVBQUMsT0FIUjtBQUlFLFlBQUksRUFBQztBQUpQO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0F4QkYsZUE4QkU7QUFBTSxXQUFHLEVBQUMsVUFBVjtBQUFxQixZQUFJLEVBQUM7QUFBMUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQTlCRixlQStCRTtBQUNFLFdBQUcsRUFBQyxXQUROO0FBRUUsWUFBSSxFQUFDLG9DQUZQO0FBR0UsYUFBSyxFQUFDO0FBSFI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQS9CRixlQW9DRTtBQUFNLFlBQUksRUFBQyx5QkFBWDtBQUFxQyxlQUFPLEVBQUM7QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXBDRixlQXFDRTtBQUFNLFlBQUksRUFBQyxhQUFYO0FBQXlCLGVBQU8sRUFBQztBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBckNGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGLGVBeUNFLHFFQUFDLDZEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUF6Q0YsZUEwQ0UscUVBQUMsK0RBQUQ7QUFBZSxXQUFLLEVBQUVLLHFEQUF0QjtBQUFBLDZCQUNFLHFFQUFDLHNEQUFEO0FBQUEsK0JBQ0UscUVBQUMsU0FBRCxvQkFBZVAsU0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUExQ0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFrREQ7QUFFREgsS0FBSyxDQUFDVyxTQUFOLEdBQWtCO0FBQ2hCVCxXQUFTLEVBQUVVLGlEQUFTLENBQUNDLFdBQVYsQ0FBc0JDLFVBRGpCO0FBRWhCWCxXQUFTLEVBQUVTLGlEQUFTLENBQUNHLE1BQVYsQ0FBaUJEO0FBRlosQ0FBbEIsQzs7Ozs7Ozs7Ozs7O0FDeEVBO0FBQUE7QUFBQTtBQUFBO0FBRUEsTUFBTUosS0FBSyxHQUFHTSx3RUFBYyxDQUFDO0FBQzNCQyxTQUFPLEVBQUU7QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUMsUUFBSSxFQUFFLE9BUEM7QUFTUEMsV0FBTyxFQUFFO0FBQ1BDLFVBQUksRUFBRTtBQURDLEtBVEY7QUFZUEMsYUFBUyxFQUFFO0FBQ1RELFVBQUksRUFBRTtBQURHLEtBWko7QUFlUEUsUUFBSSxFQUFFO0FBQ0pILGFBQU8sRUFBRSxlQURMO0FBRUpFLGVBQVMsRUFBRTtBQUZQLEtBZkM7QUFtQlBFLFdBQU8sRUFBRTtBQW5CRixHQURrQjtBQXVCM0JDLFlBQVUsRUFBRTtBQUNWQyxNQUFFLEVBQUU7QUFDRkMsZ0JBQVUsRUFBRSxZQURWO0FBRUZDLGNBQVEsRUFBRSxRQUZSO0FBR0ZDLGdCQUFVLEVBQUUsR0FIVjtBQUlGQyxtQkFBYSxFQUFFO0FBSmIsS0FETTtBQU9WQyxNQUFFLEVBQUU7QUFDRkosZ0JBQVUsRUFBRSxZQURWO0FBRUZDLGNBQVEsRUFBRSxNQUZSO0FBR0ZDLGdCQUFVLEVBQUUsR0FIVjtBQUlGQyxtQkFBYSxFQUFFO0FBSmIsS0FQTTtBQWFWRSxNQUFFLEVBQUU7QUFDRkwsZ0JBQVUsRUFBRSxZQURWO0FBRUZDLGNBQVEsRUFBRSxRQUZSO0FBR0ZDLGdCQUFVLEVBQUUsR0FIVjtBQUlGQyxtQkFBYSxFQUFFO0FBSmIsS0FiTTtBQW9CVkcsTUFBRSxFQUFFO0FBQ0ZOLGdCQUFVLEVBQUUsWUFEVjtBQUVGQyxjQUFRLEVBQUUsUUFGUjtBQUdGQyxnQkFBVSxFQUFFLEdBSFY7QUFJRkMsbUJBQWEsRUFBRTtBQUpiLEtBcEJNO0FBMkJWSSxNQUFFLEVBQUU7QUFDRlAsZ0JBQVUsRUFBRSxZQURWO0FBRUZDLGNBQVEsRUFBRSxRQUZSO0FBR0ZDLGdCQUFVLEVBQUUsR0FIVjtBQUlGQyxtQkFBYSxFQUFFO0FBSmIsS0EzQk07QUFpQ1ZLLFNBQUssRUFBRTtBQUNMUixnQkFBVSxFQUFFLFlBRFA7QUFFTEMsY0FBUSxFQUFFLE1BRkw7QUFHTEUsbUJBQWEsRUFBRTtBQUhWLEtBakNHO0FBdUNWTSxVQUFNLEVBQUU7QUFDTlQsZ0JBQVUsRUFBRSxZQUROO0FBRU5DLGNBQVEsRUFBRSxRQUZKO0FBR05FLG1CQUFhLEVBQUUsUUFIVDtBQUlOTyxtQkFBYSxFQUFFO0FBSlQ7QUF2Q0UsR0F2QmU7QUFxRTNCQyxTQUFPLEVBQUUsQ0FDUCxzQ0FETyxFQUVQLHdDQUZPLEVBR1Asd0NBSE8sRUFJUCx3Q0FKTyxFQUtQLHdDQUxPLEVBTVAsd0NBTk8sRUFPUCx3Q0FQTyxFQVFQLHdDQVJPLEVBU1Asd0NBVE8sRUFVUCx3Q0FWTyxFQVdQLHdDQVhPLEVBWVAseUNBWk8sRUFhUCx5Q0FiTztBQXJFa0IsQ0FBRCxDQUE1QjtBQXNGZTNCLG9FQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEZBLDhDOzs7Ozs7Ozs7OztBQ0FBLHlDOzs7Ozs7Ozs7OztBQ0FBLDBDOzs7Ozs7Ozs7OztBQ0FBLCtDOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLHVDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLGtEIiwiZmlsZSI6InBhZ2VzL19hcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHJlcXVpcmUoJy4uL3Nzci1tb2R1bGUtY2FjaGUuanMnKTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0dmFyIHRocmV3ID0gdHJ1ZTtcbiBcdFx0dHJ5IHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbiBcdFx0XHR0aHJldyA9IGZhbHNlO1xuIFx0XHR9IGZpbmFsbHkge1xuIFx0XHRcdGlmKHRocmV3KSBkZWxldGUgaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdH1cblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCwgY3JlYXRlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgY29va2llIGZyb20gXCJqcy1jb29raWVcIjtcclxuXHJcbmltcG9ydCB7IGNyZWF0ZVVzZXIgfSBmcm9tIFwiQGxpYi9kYlwiO1xyXG5pbXBvcnQgZmlyZWJhc2UgZnJvbSBcIkBsaWIvZmlyZWJhc2VcIjtcclxuXHJcbmNvbnN0IGF1dGhDb250ZXh0ID0gY3JlYXRlQ29udGV4dCgpO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIEF1dGhQcm92aWRlcih7IGNoaWxkcmVuIH0pIHtcclxuICBjb25zdCBhdXRoID0gdXNlUHJvdmlkZUF1dGgoKTtcclxuICByZXR1cm4gPGF1dGhDb250ZXh0LlByb3ZpZGVyIHZhbHVlPXthdXRofT57Y2hpbGRyZW59PC9hdXRoQ29udGV4dC5Qcm92aWRlcj47XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCB1c2VBdXRoID0gKCkgPT4ge1xyXG4gIHJldHVybiB1c2VDb250ZXh0KGF1dGhDb250ZXh0KTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIHVzZVByb3ZpZGVBdXRoKCkge1xyXG4gIGNvbnN0IFt1c2VyLCBzZXRVc2VyXSA9IHVzZVN0YXRlKG51bGwpO1xyXG5cclxuICBjb25zdCBoYW5kbGVVc2VyID0gYXN5bmMgKHJhd1VzZXIpID0+IHtcclxuICAgIGNvbnNvbGUubG9nKHJhd1VzZXIpO1xyXG4gICAgaWYgKHJhd1VzZXIpIHtcclxuICAgICAgY29uc3QgeyB0b2tlbiwgLi4udXNlckRhdGFXaXRob3V0VG9rZW4gfSA9IGZvcm1hdFVzZXIocmF3VXNlcik7XHJcbiAgICAgIGNvb2tpZS5zZXQoXCJ2bGFtZXItYXV0aFwiLCB0cnVlLCB7IGV4cGlyZXM6IDEgfSk7XHJcbiAgICAgIGF3YWl0IGNyZWF0ZVVzZXIodXNlckRhdGFXaXRob3V0VG9rZW4udWlkLCB1c2VyRGF0YVdpdGhvdXRUb2tlbik7XHJcbiAgICAgIHNldFVzZXIoeyAuLi51c2VyRGF0YVdpdGhvdXRUb2tlbiwgdG9rZW46IHRva2VuIH0pO1xyXG5cclxuICAgICAgcmV0dXJuIHVzZXI7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjb29raWUucmVtb3ZlKFwidmxhbWVyLWF1dGhcIik7XHJcbiAgICAgIHNldFVzZXIoZmFsc2UpO1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgY29uc3Qgc2lnbmluV2l0aEdpdGh1YiA9IGFzeW5jIChlbWFpbCwgcGFzc3dvcmQpID0+IHtcclxuICAgIHJldHVybiBhd2FpdCBmaXJlYmFzZVxyXG4gICAgICAuYXV0aCgpXHJcbiAgICAgIC5zaWduSW5XaXRoUG9wdXAobmV3IGZpcmViYXNlLmF1dGguR2l0aHViQXV0aFByb3ZpZGVyKCkpXHJcbiAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgIGhhbmRsZVVzZXIocmVzcG9uc2UudXNlcik7XHJcbiAgICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IHNpZ25pbldpdGhHb29nbGUgPSBhc3luYyAoZW1haWwsIHBhc3N3b3JkKSA9PiB7XHJcbiAgICByZXR1cm4gYXdhaXQgZmlyZWJhc2VcclxuICAgICAgLmF1dGgoKVxyXG4gICAgICAuc2lnbkluV2l0aFBvcHVwKG5ldyBmaXJlYmFzZS5hdXRoLkdvb2dsZUF1dGhQcm92aWRlcigpKVxyXG4gICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICBoYW5kbGVVc2VyKHJlc3BvbnNlLnVzZXIpO1xyXG4gICAgICB9KTtcclxuICB9O1xyXG5cclxuICBjb25zdCBzaWdub3V0ID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgcmV0dXJuIGF3YWl0IGZpcmViYXNlXHJcbiAgICAgIC5hdXRoKClcclxuICAgICAgLnNpZ25PdXQoKVxyXG4gICAgICAudGhlbigoKSA9PiB7XHJcbiAgICAgICAgaGFuZGxlVXNlcihmYWxzZSk7XHJcbiAgICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBjb25zdCB1bnN1YnNjcmliZSA9IGZpcmViYXNlLmF1dGgoKS5vbkF1dGhTdGF0ZUNoYW5nZWQoaGFuZGxlVXNlcik7XHJcblxyXG4gICAgcmV0dXJuICgpID0+IHVuc3Vic2NyaWJlKCk7XHJcbiAgfSwgW10pO1xyXG5cclxuICByZXR1cm4ge1xyXG4gICAgdXNlcixcclxuICAgIHNpZ25pbldpdGhHaXRodWIsXHJcbiAgICBzaWduaW5XaXRoR29vZ2xlLFxyXG4gICAgc2lnbm91dCxcclxuICB9O1xyXG59XHJcblxyXG5jb25zdCBmb3JtYXRVc2VyID0gKHVzZXIpID0+IHtcclxuICByZXR1cm4ge1xyXG4gICAgdWlkOiB1c2VyLnVpZCxcclxuICAgIGVtYWlsOiB1c2VyLmVtYWlsLFxyXG4gICAgbmFtZTogdXNlci5kaXNwbGF5TmFtZSxcclxuICAgIHVzZXJuYW1lOiB1c2VyLmRpc3BsYXlOYW1lLnN1YnN0cigwLCAyKSArIHVzZXIudWlkLnN1YnN0cigwLCA0KSxcclxuICAgIHRva2VuOiB1c2VyLnlhLFxyXG4gICAgcHJvdmlkZXI6IHVzZXIucHJvdmlkZXJEYXRhWzBdLnByb3ZpZGVySWQsXHJcbiAgICBpbWdVcmw6IHVzZXIucHJvdmlkZXJEYXRhWzBdLnBob3RvVVJMLFxyXG4gIH07XHJcbn07XHJcbiIsImltcG9ydCBmaXJlYmFzZSBmcm9tIFwiQGxpYi9maXJlYmFzZVwiO1xyXG5cclxuY29uc3QgZmlyZXN0b3JlID0gZmlyZWJhc2UuZmlyZXN0b3JlKCk7XHJcblxyXG5leHBvcnQgY29uc3QgY3JlYXRlVXNlciA9IGFzeW5jICh1aWQsIGRhdGEpID0+IHtcclxuICByZXR1cm4gZmlyZXN0b3JlXHJcbiAgICAuY29sbGVjdGlvbihcInVzZXJzXCIpXHJcbiAgICAuZG9jKHVpZClcclxuICAgIC5zZXQoeyAuLi5kYXRhIH0sIHsgbWVyZ2U6IHRydWUgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0QWxsVXNlcnMgPSBhc3luYyAoKSA9PiB7XHJcbiAgY29uc3Qgc25hcHNob3QgPSBhd2FpdCBmaXJlc3RvcmUuY29sbGVjdGlvbihcInVzZXJzXCIpLmdldCgpO1xyXG4gIGxldCByZXNwb25zZSA9IHt9O1xyXG4gIHNuYXBzaG90LmZvckVhY2goKGRvYykgPT4ge1xyXG4gICAgcmVzcG9uc2VbZG9jLmlkXSA9IHtcclxuICAgICAgLi4uZG9jLmRhdGEoKSxcclxuICAgIH07XHJcbiAgfSk7XHJcbiAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gIHJldHVybiByZXNwb25zZTtcclxufTtcclxuIiwiaW1wb3J0ICogYXMgZmlyZWJhc2UgZnJvbSBcImZpcmViYXNlL2FwcFwiO1xyXG5pbXBvcnQgXCJmaXJlYmFzZS9hdXRoXCI7XHJcbmltcG9ydCBcImZpcmViYXNlL2ZpcmVzdG9yZVwiO1xyXG5cclxuaWYgKCFmaXJlYmFzZS5kZWZhdWx0LmFwcHMubGVuZ3RoKSB7XHJcbiAgZmlyZWJhc2UuZGVmYXVsdC5pbml0aWFsaXplQXBwKHtcclxuICAgIGFwaUtleTogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfRklSRUJBU0VfQVBJX0tFWSxcclxuICAgIGF1dGhEb21haW46IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0ZJUkVCQVNFX0FVVEhfRE9NQUlOLFxyXG4gICAgcHJvamVjdElkOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19GSVJFQkFTRV9QUk9KRUNUX0lELFxyXG4gIH0pO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmaXJlYmFzZS5kZWZhdWx0O1xyXG4iLCIvLyBDb3JlIGNvbXBvbmVudHNcclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gXCJwcm9wLXR5cGVzXCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IHsgUGFwZXIsIENzc0Jhc2VsaW5lLCBUaGVtZVByb3ZpZGVyIH0gZnJvbSBcIkBtYXRlcmlhbC11aS9jb3JlXCI7XHJcblxyXG4vLyBDb21wb25lbnRzXHJcbmltcG9ydCB0aGVtZSBmcm9tIFwiQHN0eWxlcy9UaGVtZVwiO1xyXG5pbXBvcnQgeyBBdXRoUHJvdmlkZXIgfSBmcm9tIFwiQGxpYi9hdXRoXCI7XHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIE15QXBwKHByb3BzKSB7XHJcbiAgY29uc3QgeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9ID0gcHJvcHM7XHJcblxyXG4gIFJlYWN0LnVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAvLyBSZW1vdmUgdGhlIHNlcnZlci1zaWRlIGluamVjdGVkIENTUy5cclxuICAgIGNvbnN0IGpzc1N0eWxlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjanNzLXNlcnZlci1zaWRlXCIpO1xyXG4gICAgaWYgKGpzc1N0eWxlcykge1xyXG4gICAgICBqc3NTdHlsZXMucGFyZW50RWxlbWVudC5yZW1vdmVDaGlsZChqc3NTdHlsZXMpO1xyXG4gICAgfVxyXG4gIH0sIFtdKTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxQYXBlcj5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgPG1ldGEgaHR0cD0nQ29udGVudC1UeXBlJyBjb250ZW50PSd0ZXh0L2h0bWw7IGNoYXJTZXQ9dXRmLTgnIC8+XHJcbiAgICAgICAgPHRpdGxlPkFiZWwncyByZXN1bWU8L3RpdGxlPlxyXG4gICAgICAgIDxtZXRhXHJcbiAgICAgICAgICBjb250ZW50PSdUaGlzIGlzIHBlcnNvbmFsIHBvcnRmb2xpbyBzaXRlIHRvIHNob3djYXNlIHRoZSBwcm9qZWN0cyB0aGF0IGFiZWwgaGFzIGJlZW4gYSBwYXJ0IG9mLiAnXHJcbiAgICAgICAgICBuYW1lPSdkZXNjcmlwdGlvbidcclxuICAgICAgICAvPlxyXG4gICAgICAgIDxtZXRhIGNvbnRlbnQ9J3dpZHRoPWRldmljZS13aWR0aCwgaW5pdGlhbC1zY2FsZT0xJyBuYW1lPSd2aWV3cG9ydCcgLz5cclxuICAgICAgICA8bWV0YSBodHRwPSdDb250ZW50LVR5cGUnIGNvbnRlbnQ9J3RleHQvaHRtbDsgY2hhclNldD11dGYtOCcgLz5cclxuICAgICAgICA8bWV0YSBjb250ZW50PSd3aWR0aD1kZXZpY2Utd2lkdGgsIGluaXRpYWwtc2NhbGU9MScgbmFtZT0ndmlld3BvcnQnIC8+XHJcbiAgICAgICAgPG1ldGEgbmFtZT0nbXNhcHBsaWNhdGlvbi1UaWxlQ29sb3InIGNvbnRlbnQ9JyMyZDg5ZWYnIC8+XHJcbiAgICAgICAgPG1ldGEgbmFtZT0ndGhlbWUtY29sb3InIGNvbnRlbnQ9JyNmZmZmZmYnIC8+XHJcblxyXG4gICAgICAgIDxsaW5rXHJcbiAgICAgICAgICByZWw9J2FwcGxlLXRvdWNoLWljb24nXHJcbiAgICAgICAgICBzaXplcz0nMTgweDE4MCdcclxuICAgICAgICAgIGhyZWY9Jy9pbWcvZmF2aWNvbi9hcHBsZS10b3VjaC1pY29uLnBuZydcclxuICAgICAgICAvPlxyXG4gICAgICAgIDxsaW5rXHJcbiAgICAgICAgICByZWw9J2ljb24nXHJcbiAgICAgICAgICB0eXBlPSdpbWFnZS9wbmcnXHJcbiAgICAgICAgICBzaXplcz0nMzJ4MzInXHJcbiAgICAgICAgICBocmVmPScvaW1nL2Zhdmljb24vZmF2aWNvbi0zMngzMi5wbmcnXHJcbiAgICAgICAgLz5cclxuICAgICAgICA8bGlua1xyXG4gICAgICAgICAgcmVsPSdpY29uJ1xyXG4gICAgICAgICAgdHlwZT0naW1hZ2UvcG5nJ1xyXG4gICAgICAgICAgc2l6ZXM9JzE2eDE2J1xyXG4gICAgICAgICAgaHJlZj0nL2ltZy9mYXZpY29uL2Zhdmljb24tMTZ4MTYucG5nJ1xyXG4gICAgICAgIC8+XHJcbiAgICAgICAgPGxpbmsgcmVsPSdtYW5pZmVzdCcgaHJlZj0nL2ltZy9mYXZpY29uL3NpdGUud2VibWFuaWZlc3QnIC8+XHJcbiAgICAgICAgPGxpbmtcclxuICAgICAgICAgIHJlbD0nbWFzay1pY29uJ1xyXG4gICAgICAgICAgaHJlZj0nL2ltZy9mYXZpY29uL3NhZmFyaS1waW5uZWQtdGFiLnN2ZydcclxuICAgICAgICAgIGNvbG9yPScjNWJiYWQ1J1xyXG4gICAgICAgIC8+XHJcbiAgICAgICAgPG1ldGEgbmFtZT0nbXNhcHBsaWNhdGlvbi1UaWxlQ29sb3InIGNvbnRlbnQ9JyNkYTUzMmMnIC8+XHJcbiAgICAgICAgPG1ldGEgbmFtZT0ndGhlbWUtY29sb3InIGNvbnRlbnQ9JyNmZmZmZmYnIC8+XHJcbiAgICAgIDwvSGVhZD5cclxuXHJcbiAgICAgIDxDc3NCYXNlbGluZSAvPlxyXG4gICAgICA8VGhlbWVQcm92aWRlciB0aGVtZT17dGhlbWV9PlxyXG4gICAgICAgIDxBdXRoUHJvdmlkZXI+XHJcbiAgICAgICAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XHJcbiAgICAgICAgPC9BdXRoUHJvdmlkZXI+XHJcbiAgICAgIDwvVGhlbWVQcm92aWRlcj5cclxuICAgIDwvUGFwZXI+XHJcbiAgKTtcclxufVxyXG5cclxuTXlBcHAucHJvcFR5cGVzID0ge1xyXG4gIENvbXBvbmVudDogUHJvcFR5cGVzLmVsZW1lbnRUeXBlLmlzUmVxdWlyZWQsXHJcbiAgcGFnZVByb3BzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXHJcbn07XHJcbiIsImltcG9ydCB7IGNyZWF0ZU11aVRoZW1lIH0gZnJvbSBcIkBtYXRlcmlhbC11aS9jb3JlXCI7XHJcblxyXG5jb25zdCB0aGVtZSA9IGNyZWF0ZU11aVRoZW1lKHtcclxuICBwYWxldHRlOiB7XHJcbiAgICAvLyAgIGJhY2tncm91bmQ6IHtcclxuICAgIC8vICAgICBwYXBlcjogbW9kZSA9PT0gXCJsaWdodFwiID8gXCIjRkZGXCIgOiBcIiMwMDBcIixcclxuICAgIC8vICAgICBkZWZhdWx0OiBtb2RlID09PSBcImxpZ2h0XCIgPyBcIiNGRkZcIiA6IFwiIzAwMFwiLFxyXG4gICAgLy8gICAgIGxldmVsMjogbW9kZSA9PT0gXCJsaWdodFwiID8gXCIjRjVmNWY1XCIgOiBcIiMwNTA1MDVcIixcclxuICAgIC8vICAgICBsZXZlbDE6IG1vZGUgPT09IFwibGlnaHRcIiA/IFwiI0ZGRlwiIDogXCIjMDAwXCIsXHJcbiAgICAvLyAgIH0sXHJcbiAgICB0eXBlOiBcImxpZ2h0XCIsXHJcblxyXG4gICAgcHJpbWFyeToge1xyXG4gICAgICBtYWluOiBcIiMyYzI5MjVcIixcclxuICAgIH0sXHJcbiAgICBzZWNvbmRhcnk6IHtcclxuICAgICAgbWFpbjogXCIjNzNlOGZmXCIsXHJcbiAgICB9LFxyXG4gICAgdGV4dDoge1xyXG4gICAgICBwcmltYXJ5OiBcInJnYmEoMCwgMCwgMClcIixcclxuICAgICAgc2Vjb25kYXJ5OiBcInJnYmEoMjU1LCAyNTUsIDI1NSlcIixcclxuICAgIH0sXHJcbiAgICBkaXZpZGVyOiBcIiM3M2U4ZmZcIixcclxuICB9LFxyXG5cclxuICB0eXBvZ3JhcGh5OiB7XHJcbiAgICBoMToge1xyXG4gICAgICBmb250RmFtaWx5OiBcIkNvb2x2ZXRpY2FcIixcclxuICAgICAgZm9udFNpemU6IFwiMy41cmVtXCIsXHJcbiAgICAgIGZvbnRXZWlnaHQ6IDYwMCxcclxuICAgICAgbGV0dGVyU3BhY2luZzogXCIwLjJyZW1cIixcclxuICAgIH0sXHJcbiAgICBoMjoge1xyXG4gICAgICBmb250RmFtaWx5OiBcIkNvb2x2ZXRpY2FcIixcclxuICAgICAgZm9udFNpemU6IFwiM3JlbVwiLFxyXG4gICAgICBmb250V2VpZ2h0OiA0MDAsXHJcbiAgICAgIGxldHRlclNwYWNpbmc6IFwiMC4xcmVtXCIsXHJcbiAgICB9LFxyXG4gICAgaDM6IHtcclxuICAgICAgZm9udEZhbWlseTogXCJNb250c2VycmF0XCIsXHJcbiAgICAgIGZvbnRTaXplOiBcIjIuMnJlbVwiLFxyXG4gICAgICBmb250V2VpZ2h0OiAzMDAsXHJcbiAgICAgIGxldHRlclNwYWNpbmc6IFwiMC4xcmVtXCIsXHJcbiAgICB9LFxyXG5cclxuICAgIGg1OiB7XHJcbiAgICAgIGZvbnRGYW1pbHk6IFwiQ29vbHZldGljYVwiLFxyXG4gICAgICBmb250U2l6ZTogXCIxLjVyZW1cIixcclxuICAgICAgZm9udFdlaWdodDogMzAwLFxyXG4gICAgICBsZXR0ZXJTcGFjaW5nOiBcIjAuMXJlbVwiLFxyXG4gICAgfSxcclxuXHJcbiAgICBoNjoge1xyXG4gICAgICBmb250RmFtaWx5OiBcIkNvb2x2ZXRpY2FcIixcclxuICAgICAgZm9udFNpemU6IFwiMS4ycmVtXCIsXHJcbiAgICAgIGZvbnRXZWlnaHQ6IDMwMCxcclxuICAgICAgbGV0dGVyU3BhY2luZzogXCIwLjFyZW1cIixcclxuICAgIH0sXHJcbiAgICBib2R5MToge1xyXG4gICAgICBmb250RmFtaWx5OiBcIk1vbnRzZXJyYXRcIixcclxuICAgICAgZm9udFNpemU6IFwiMXJlbVwiLFxyXG4gICAgICBsZXR0ZXJTcGFjaW5nOiBcIjAuMXJlbVwiLFxyXG4gICAgfSxcclxuXHJcbiAgICBidXR0b246IHtcclxuICAgICAgZm9udEZhbWlseTogXCJNb250c2VycmF0XCIsXHJcbiAgICAgIGZvbnRTaXplOiBcIjAuOHJlbVwiLFxyXG4gICAgICBsZXR0ZXJTcGFjaW5nOiBcIjAuMXJlbVwiLFxyXG4gICAgICB0ZXh0VHJhbnNmb3JtOiBcImNhcGl0YWxpemVcIixcclxuICAgIH0sXHJcbiAgfSxcclxuICBzaGFkb3dzOiBbXHJcbiAgICBcIjBweCAwcHggMHB4IDBweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggMHB4IDEwcHggLTVweCByZ2JhKDAsIDI1NSwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggMHB4IDEycHggLTRweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggMnB4IDE0cHggLTNweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggMnB4IDE2cHggLTVweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggNHB4IDE4cHggLTRweCByZ2JhKDAsIDI1NSwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggNHB4IDIwcHggLTNweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggNnB4IDIycHggLTVweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggNnB4IDI0cHggLTRweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggOHB4IDI2cHggLTNweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggOHB4IDI4cHggLTVweCByZ2JhKDAsIDEyMCwgMCwgMC4zKVwiLFxyXG4gICAgXCIwcHggMTBweCAzMHB4IC00cHggcmdiYSgwLCAxMjAsIDAsIDAuMylcIixcclxuICAgIFwiMHB4IDEwcHggMzJweCAtM3B4IHJnYmEoMCwgMTIwLCAwLCAwLjMpXCIsXHJcbiAgXSxcclxufSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCB0aGVtZTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2NvcmVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlyZWJhc2UvYXBwXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImZpcmViYXNlL2F1dGhcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlyZWJhc2UvZmlyZXN0b3JlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImpzLWNvb2tpZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L2hlYWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==