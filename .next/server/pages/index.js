module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Login.js":
/*!*****************************!*\
  !*** ./components/Login.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "classnames");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/colors */ "@material-ui/core/colors");
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-icons/fi */ "react-icons/fi");
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_icons_fi__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_icons_fc__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-icons/fc */ "react-icons/fc");
/* harmony import */ var react_icons_fc__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_icons_fc__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-icons/fa */ "react-icons/fa");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_icons_fa__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _lib_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @lib/auth */ "./lib/auth.js");

var _jsxFileName = "D:\\Development\\Full-stack\\vlamer\\components\\Login.js";





 // Components


const useStyle = Object(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["makeStyles"])(theme => ({
  container: {
    width: "90vw",
    margin: "40% auto",
    padding: "3rem",
    boxShadow: theme.shadows[8]
  },
  button: {
    margin: "1.5rem 0rem",
    padding: `${theme.spacing(2)}px ${theme.spacing(0)}px`,
    letterSpacing: "3px" // backgroundColor: "inherit",

  },
  icon: {
    fontSize: theme.spacing(3),
    margin: `0rem ${theme.spacing(1)}px`
  },
  button_github: {
    backgroundColor: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__["grey"][900],
    color: "white",
    "&:active": {
      backgroundColor: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__["grey"][800],
      transform: "scale(0.95)"
    },
    "&:hover": {
      backgroundColor: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__["grey"][800]
    }
  },
  button_google: {
    backgroundColor: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__["grey"][100],
    color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__["grey"][700],
    "&:active": {
      backgroundColor: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__["grey"][200],
      transform: "scale(0.95)"
    },
    "&:hover": {
      backgroundColor: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__["grey"][200]
    }
  },
  divider: {
    margin: "1rem 0rem"
  },
  more: {
    textAlign: "center"
  }
}));

const Login = params => {
  const classes = useStyle();
  const auth = Object(_lib_auth__WEBPACK_IMPORTED_MODULE_7__["useAuth"])();

  const handleLogin = target => {
    if (target === "github_button") {
      auth.signinWithGithub();
    } else if (target === "google_button") {
      auth.signinWithGoogle();
    } else if (target === "twitter_button") {//   console.log("TWITTER");
    } else if (target === "facebook_button") {//   console.log("FACEBOOK");
    } else if (target === "apple_button") {//   console.log("APPLE");
    }
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    container: true,
    direction: "column",
    className: classes.container,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Button"], {
      id: "github_button",
      variant: "contained",
      className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(classes.button, classes.button_github),
      onClick: () => handleLogin("github_button"),
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_icons_fi__WEBPACK_IMPORTED_MODULE_4__["FiGithub"], {
        className: classes.icon
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 97,
        columnNumber: 9
      }, undefined), "Sign in with Github"]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 91,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Button"], {
      id: "google_button",
      variant: "contained",
      className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(classes.button, classes.button_google),
      onClick: () => handleLogin("google_button"),
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_icons_fc__WEBPACK_IMPORTED_MODULE_5__["FcGoogle"], {
        className: classes.icon
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 106,
        columnNumber: 9
      }, undefined), "Sign in with Google"]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Divider"], {
      className: classes.divider
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 110,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
      variant: "subtitle2",
      className: classes.more,
      children: "Others"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 111,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
      container: true,
      justify: "space-around",
      spacing: 2,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["IconButton"], {
        id: "facebook_button",
        onClick: () => handleLogin("facebook_button"),
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_icons_fi__WEBPACK_IMPORTED_MODULE_4__["FiFacebook"], {
          className: classes.icon
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 119,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 115,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["IconButton"], {
        id: "twitter_button",
        className: classes.button,
        onClick: () => handleLogin("twitter_button"),
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_icons_fi__WEBPACK_IMPORTED_MODULE_4__["FiTwitter"], {
          className: classes.icon
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 126,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 121,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["IconButton"], {
        id: "apple_button",
        onClick: () => handleLogin("apple_button"),
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_icons_fa__WEBPACK_IMPORTED_MODULE_6__["FaApple"], {
          className: classes.icon
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 133,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 129,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 114,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 90,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (Login);

/***/ }),

/***/ "./components/WelcomeScreen.js":
/*!*************************************!*\
  !*** ./components/WelcomeScreen.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "classnames");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/colors */ "@material-ui/core/colors");
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_colors__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-icons/fi */ "react-icons/fi");
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_icons_fi__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_icons_fc__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-icons/fc */ "react-icons/fc");
/* harmony import */ var react_icons_fc__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_icons_fc__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-icons/fa */ "react-icons/fa");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_icons_fa__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _lib_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @lib/auth */ "./lib/auth.js");

var _jsxFileName = "D:\\Development\\Full-stack\\vlamer\\components\\WelcomeScreen.js";







const useStyle = Object(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["makeStyles"])(theme => ({
  container: {
    width: "90vw",
    margin: "10rem auto",
    padding: theme.spacing(2),
    boxShadow: theme.shadows[8],
    textAlign: "center"
  },
  header: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    boxShadow: theme.shadows[8]
  },
  username: {
    margin: `${theme.spacing(1)}px 0px`
  }
}));

const welcomeScreen = params => {
  const classes = useStyle();
  const auth = Object(_lib_auth__WEBPACK_IMPORTED_MODULE_7__["useAuth"])();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    container: true,
    direction: "column",
    alignContent: "center",
    spacing: 3,
    className: classes.container,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
      item: true,
      className: classes.header,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Avatar"], {
        className: classes.avatar,
        src: auth.user.imgUrl
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
        className: classes.username,
        variant: "h5",
        color: "primary",
        children: auth.user.name
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Button"], {
      variant: "outlined",
      color: "primary",
      onClick: () => auth.signout(),
      children: "Logout"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 48,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (welcomeScreen);

/***/ }),

/***/ "./lib/auth.js":
/*!*********************!*\
  !*** ./lib/auth.js ***!
  \*********************/
/*! exports provided: AuthProvider, useAuth */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthProvider", function() { return AuthProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useAuth", function() { return useAuth; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! js-cookie */ "js-cookie");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _lib_db__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @lib/db */ "./lib/db.js");
/* harmony import */ var _lib_firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @lib/firebase */ "./lib/firebase.js");

var _jsxFileName = "D:\\Development\\Full-stack\\vlamer\\lib\\auth.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }





const authContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createContext"])();
function AuthProvider({
  children
}) {
  const auth = useProvideAuth();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(authContext.Provider, {
    value: auth,
    children: children
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 11,
    columnNumber: 10
  }, this);
}
const useAuth = () => {
  return Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(authContext);
};

function useProvideAuth() {
  const {
    0: user,
    1: setUser
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);

  const handleUser = async rawUser => {
    console.log(rawUser);

    if (rawUser) {
      const _formatUser = formatUser(rawUser),
            {
        token
      } = _formatUser,
            userDataWithoutToken = _objectWithoutProperties(_formatUser, ["token"]);

      js_cookie__WEBPACK_IMPORTED_MODULE_2___default.a.set("vlamer-auth", true, {
        expires: 1
      });
      await Object(_lib_db__WEBPACK_IMPORTED_MODULE_3__["createUser"])(userDataWithoutToken.uid, userDataWithoutToken);
      setUser(_objectSpread(_objectSpread({}, userDataWithoutToken), {}, {
        token: token
      }));
      return user;
    } else {
      js_cookie__WEBPACK_IMPORTED_MODULE_2___default.a.remove("vlamer-auth");
      setUser(false);
      return false;
    }
  };

  const signinWithGithub = async (email, password) => {
    return await _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().signInWithPopup(new _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth.GithubAuthProvider()).then(response => {
      handleUser(response.user);
    });
  };

  const signinWithGoogle = async (email, password) => {
    return await _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().signInWithPopup(new _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth.GoogleAuthProvider()).then(response => {
      handleUser(response.user);
    });
  };

  const signout = async () => {
    return await _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().signOut().then(() => {
      handleUser(false);
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    const unsubscribe = _lib_firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().onAuthStateChanged(handleUser);
    return () => unsubscribe();
  }, []);
  return {
    user,
    signinWithGithub,
    signinWithGoogle,
    signout
  };
}

const formatUser = user => {
  return {
    uid: user.uid,
    email: user.email,
    name: user.displayName,
    username: user.displayName.substr(0, 2) + user.uid.substr(0, 4),
    token: user.ya,
    provider: user.providerData[0].providerId,
    imgUrl: user.providerData[0].photoURL
  };
};

/***/ }),

/***/ "./lib/db.js":
/*!*******************!*\
  !*** ./lib/db.js ***!
  \*******************/
/*! exports provided: createUser, getAllUsers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createUser", function() { return createUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllUsers", function() { return getAllUsers; });
/* harmony import */ var _lib_firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @lib/firebase */ "./lib/firebase.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const firestore = _lib_firebase__WEBPACK_IMPORTED_MODULE_0__["default"].firestore();
const createUser = async (uid, data) => {
  return firestore.collection("users").doc(uid).set(_objectSpread({}, data), {
    merge: true
  });
};
const getAllUsers = async () => {
  const snapshot = await firestore.collection("users").get();
  let response = {};
  snapshot.forEach(doc => {
    response[doc.id] = _objectSpread({}, doc.data());
  });
  console.log(response);
  return response;
};

/***/ }),

/***/ "./lib/firebase.js":
/*!*************************!*\
  !*** ./lib/firebase.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/app */ "firebase/app");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/auth */ "firebase/auth");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_auth__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/firestore */ "firebase/firestore");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_firestore__WEBPACK_IMPORTED_MODULE_2__);




if (!firebase_app__WEBPACK_IMPORTED_MODULE_0___default.a.apps.length) {
  firebase_app__WEBPACK_IMPORTED_MODULE_0___default.a.initializeApp({
    apiKey: "AIzaSyB8HWgzpTqOJCK0CixMzhBVuJYTryyRf1c",
    authDomain: "next-js-blog-89df8.firebaseapp.com",
    projectId: "next-js-blog-89df8"
  });
}

/* harmony default export */ __webpack_exports__["default"] = (firebase_app__WEBPACK_IMPORTED_MODULE_0___default.a);

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _lib_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @lib/auth */ "./lib/auth.js");
/* harmony import */ var _components_Login__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @components/Login */ "./components/Login.js");
/* harmony import */ var _components_WelcomeScreen__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @components/WelcomeScreen */ "./components/WelcomeScreen.js");


var _jsxFileName = "D:\\Development\\Full-stack\\vlamer\\pages\\index.js";


 // Components




const useStyle = Object(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["makeStyles"])(theme => ({
  container: {
    width: "100vw",
    height: "100vh"
  }
}));

const Dashboard = () => {
  const classes = useStyle();
  const auth = Object(_lib_auth__WEBPACK_IMPORTED_MODULE_4__["useAuth"])();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("script", {
        dangerouslySetInnerHTML: {
          __html: `if (document.cookie && document.cookie.includes('vlamer-auth')) {
          window.location.href = "/dashboard"
        }`
        }
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 7
    }, undefined), auth !== null && auth !== void 0 && auth.user ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: classes.container,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_WelcomeScreen__WEBPACK_IMPORTED_MODULE_6__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 11
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 9
    }, undefined) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: classes.container,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_Login__WEBPACK_IMPORTED_MODULE_5__["default"], {
        children: "Hello Vlamers"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 11
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 9
    }, undefined)]
  }, void 0, true);
};

/* harmony default export */ __webpack_exports__["default"] = (Dashboard);

/***/ }),

/***/ "@material-ui/core":
/*!************************************!*\
  !*** external "@material-ui/core" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core");

/***/ }),

/***/ "@material-ui/core/colors":
/*!*******************************************!*\
  !*** external "@material-ui/core/colors" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/colors");

/***/ }),

/***/ "classnames":
/*!*****************************!*\
  !*** external "classnames" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),

/***/ "firebase/app":
/*!*******************************!*\
  !*** external "firebase/app" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/app");

/***/ }),

/***/ "firebase/auth":
/*!********************************!*\
  !*** external "firebase/auth" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/auth");

/***/ }),

/***/ "firebase/firestore":
/*!*************************************!*\
  !*** external "firebase/firestore" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/firestore");

/***/ }),

/***/ "js-cookie":
/*!****************************!*\
  !*** external "js-cookie" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("js-cookie");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-icons/fa":
/*!*********************************!*\
  !*** external "react-icons/fa" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-icons/fa");

/***/ }),

/***/ "react-icons/fc":
/*!*********************************!*\
  !*** external "react-icons/fc" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-icons/fc");

/***/ }),

/***/ "react-icons/fi":
/*!*********************************!*\
  !*** external "react-icons/fi" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-icons/fi");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9Mb2dpbi5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1dlbGNvbWVTY3JlZW4uanMiLCJ3ZWJwYWNrOi8vLy4vbGliL2F1dGguanMiLCJ3ZWJwYWNrOi8vLy4vbGliL2RiLmpzIiwid2VicGFjazovLy8uL2xpYi9maXJlYmFzZS5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL2NvbG9yc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImNsYXNzbmFtZXNcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJmaXJlYmFzZS9hcHBcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJmaXJlYmFzZS9hdXRoXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZmlyZWJhc2UvZmlyZXN0b3JlXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwianMtY29va2llXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9oZWFkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3RcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1pY29ucy9mYVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWljb25zL2ZjXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtaWNvbnMvZmlcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiJdLCJuYW1lcyI6WyJ1c2VTdHlsZSIsIm1ha2VTdHlsZXMiLCJ0aGVtZSIsImNvbnRhaW5lciIsIndpZHRoIiwibWFyZ2luIiwicGFkZGluZyIsImJveFNoYWRvdyIsInNoYWRvd3MiLCJidXR0b24iLCJzcGFjaW5nIiwibGV0dGVyU3BhY2luZyIsImljb24iLCJmb250U2l6ZSIsImJ1dHRvbl9naXRodWIiLCJiYWNrZ3JvdW5kQ29sb3IiLCJncmV5IiwiY29sb3IiLCJ0cmFuc2Zvcm0iLCJidXR0b25fZ29vZ2xlIiwiZGl2aWRlciIsIm1vcmUiLCJ0ZXh0QWxpZ24iLCJMb2dpbiIsInBhcmFtcyIsImNsYXNzZXMiLCJhdXRoIiwidXNlQXV0aCIsImhhbmRsZUxvZ2luIiwidGFyZ2V0Iiwic2lnbmluV2l0aEdpdGh1YiIsInNpZ25pbldpdGhHb29nbGUiLCJjbHN4IiwiaGVhZGVyIiwiZGlzcGxheSIsImZsZXhEaXJlY3Rpb24iLCJhbGlnbkl0ZW1zIiwiYXZhdGFyIiwiaGVpZ2h0IiwidXNlcm5hbWUiLCJ3ZWxjb21lU2NyZWVuIiwidXNlciIsImltZ1VybCIsIm5hbWUiLCJzaWdub3V0IiwiYXV0aENvbnRleHQiLCJjcmVhdGVDb250ZXh0IiwiQXV0aFByb3ZpZGVyIiwiY2hpbGRyZW4iLCJ1c2VQcm92aWRlQXV0aCIsInVzZUNvbnRleHQiLCJzZXRVc2VyIiwidXNlU3RhdGUiLCJoYW5kbGVVc2VyIiwicmF3VXNlciIsImNvbnNvbGUiLCJsb2ciLCJmb3JtYXRVc2VyIiwidG9rZW4iLCJ1c2VyRGF0YVdpdGhvdXRUb2tlbiIsImNvb2tpZSIsInNldCIsImV4cGlyZXMiLCJjcmVhdGVVc2VyIiwidWlkIiwicmVtb3ZlIiwiZW1haWwiLCJwYXNzd29yZCIsImZpcmViYXNlIiwic2lnbkluV2l0aFBvcHVwIiwiR2l0aHViQXV0aFByb3ZpZGVyIiwidGhlbiIsInJlc3BvbnNlIiwiR29vZ2xlQXV0aFByb3ZpZGVyIiwic2lnbk91dCIsInVzZUVmZmVjdCIsInVuc3Vic2NyaWJlIiwib25BdXRoU3RhdGVDaGFuZ2VkIiwiZGlzcGxheU5hbWUiLCJzdWJzdHIiLCJ5YSIsInByb3ZpZGVyIiwicHJvdmlkZXJEYXRhIiwicHJvdmlkZXJJZCIsInBob3RvVVJMIiwiZmlyZXN0b3JlIiwiZGF0YSIsImNvbGxlY3Rpb24iLCJkb2MiLCJtZXJnZSIsImdldEFsbFVzZXJzIiwic25hcHNob3QiLCJnZXQiLCJmb3JFYWNoIiwiaWQiLCJhcHBzIiwibGVuZ3RoIiwiaW5pdGlhbGl6ZUFwcCIsImFwaUtleSIsInByb2Nlc3MiLCJhdXRoRG9tYWluIiwicHJvamVjdElkIiwiTkVYVF9QVUJMSUNfRklSRUJBU0VfUFJPSkVDVF9JRCIsIkRhc2hib2FyZCIsIl9faHRtbCJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUNBO0FBUUE7QUFFQTtBQUNBO0NBR0E7O0FBQ0E7QUFFQSxNQUFNQSxRQUFRLEdBQUdDLG9FQUFVLENBQUVDLEtBQUQsS0FBWTtBQUN0Q0MsV0FBUyxFQUFFO0FBQ1RDLFNBQUssRUFBRSxNQURFO0FBRVRDLFVBQU0sRUFBRSxVQUZDO0FBR1RDLFdBQU8sRUFBRSxNQUhBO0FBSVRDLGFBQVMsRUFBRUwsS0FBSyxDQUFDTSxPQUFOLENBQWMsQ0FBZDtBQUpGLEdBRDJCO0FBUXRDQyxRQUFNLEVBQUU7QUFDTkosVUFBTSxFQUFFLGFBREY7QUFFTkMsV0FBTyxFQUFHLEdBQUVKLEtBQUssQ0FBQ1EsT0FBTixDQUFjLENBQWQsQ0FBaUIsTUFBS1IsS0FBSyxDQUFDUSxPQUFOLENBQWMsQ0FBZCxDQUFpQixJQUY3QztBQUdOQyxpQkFBYSxFQUFFLEtBSFQsQ0FJTjs7QUFKTSxHQVI4QjtBQWV0Q0MsTUFBSSxFQUFFO0FBQ0pDLFlBQVEsRUFBRVgsS0FBSyxDQUFDUSxPQUFOLENBQWMsQ0FBZCxDQUROO0FBRUpMLFVBQU0sRUFBRyxRQUFPSCxLQUFLLENBQUNRLE9BQU4sQ0FBYyxDQUFkLENBQWlCO0FBRjdCLEdBZmdDO0FBb0J0Q0ksZUFBYSxFQUFFO0FBQ2JDLG1CQUFlLEVBQUVDLDZEQUFJLENBQUMsR0FBRCxDQURSO0FBRWJDLFNBQUssRUFBRSxPQUZNO0FBSWIsZ0JBQVk7QUFDVkYscUJBQWUsRUFBRUMsNkRBQUksQ0FBQyxHQUFELENBRFg7QUFFVkUsZUFBUyxFQUFFO0FBRkQsS0FKQztBQVFiLGVBQVc7QUFDVEgscUJBQWUsRUFBRUMsNkRBQUksQ0FBQyxHQUFEO0FBRFo7QUFSRSxHQXBCdUI7QUFpQ3RDRyxlQUFhLEVBQUU7QUFDYkosbUJBQWUsRUFBRUMsNkRBQUksQ0FBQyxHQUFELENBRFI7QUFFYkMsU0FBSyxFQUFFRCw2REFBSSxDQUFDLEdBQUQsQ0FGRTtBQUdiLGdCQUFZO0FBQ1ZELHFCQUFlLEVBQUVDLDZEQUFJLENBQUMsR0FBRCxDQURYO0FBRVZFLGVBQVMsRUFBRTtBQUZELEtBSEM7QUFPYixlQUFXO0FBQ1RILHFCQUFlLEVBQUVDLDZEQUFJLENBQUMsR0FBRDtBQURaO0FBUEUsR0FqQ3VCO0FBNkN0Q0ksU0FBTyxFQUFFO0FBQ1BmLFVBQU0sRUFBRTtBQURELEdBN0M2QjtBQWdEdENnQixNQUFJLEVBQUU7QUFDSkMsYUFBUyxFQUFFO0FBRFA7QUFoRGdDLENBQVosQ0FBRCxDQUEzQjs7QUFxREEsTUFBTUMsS0FBSyxHQUFJQyxNQUFELElBQVk7QUFDeEIsUUFBTUMsT0FBTyxHQUFHekIsUUFBUSxFQUF4QjtBQUNBLFFBQU0wQixJQUFJLEdBQUdDLHlEQUFPLEVBQXBCOztBQUVBLFFBQU1DLFdBQVcsR0FBSUMsTUFBRCxJQUFZO0FBQzlCLFFBQUlBLE1BQU0sS0FBSyxlQUFmLEVBQWdDO0FBQzlCSCxVQUFJLENBQUNJLGdCQUFMO0FBQ0QsS0FGRCxNQUVPLElBQUlELE1BQU0sS0FBSyxlQUFmLEVBQWdDO0FBQ3JDSCxVQUFJLENBQUNLLGdCQUFMO0FBQ0QsS0FGTSxNQUVBLElBQUlGLE1BQU0sS0FBSyxnQkFBZixFQUFpQyxDQUN0QztBQUNELEtBRk0sTUFFQSxJQUFJQSxNQUFNLEtBQUssaUJBQWYsRUFBa0MsQ0FDdkM7QUFDRCxLQUZNLE1BRUEsSUFBSUEsTUFBTSxLQUFLLGNBQWYsRUFBK0IsQ0FDcEM7QUFDRDtBQUNGLEdBWkQ7O0FBYUEsc0JBQ0UscUVBQUMsc0RBQUQ7QUFBTSxhQUFTLE1BQWY7QUFBZ0IsYUFBUyxFQUFDLFFBQTFCO0FBQW1DLGFBQVMsRUFBRUosT0FBTyxDQUFDdEIsU0FBdEQ7QUFBQSw0QkFDRSxxRUFBQyx3REFBRDtBQUNFLFFBQUUsRUFBQyxlQURMO0FBRUUsYUFBTyxFQUFDLFdBRlY7QUFHRSxlQUFTLEVBQUU2QixpREFBSSxDQUFDUCxPQUFPLENBQUNoQixNQUFULEVBQWlCZ0IsT0FBTyxDQUFDWCxhQUF6QixDQUhqQjtBQUlFLGFBQU8sRUFBRSxNQUFNYyxXQUFXLENBQUMsZUFBRCxDQUo1QjtBQUFBLDhCQU1FLHFFQUFDLHVEQUFEO0FBQVUsaUJBQVMsRUFBRUgsT0FBTyxDQUFDYjtBQUE3QjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQU5GO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERixlQVVFLHFFQUFDLHdEQUFEO0FBQ0UsUUFBRSxFQUFDLGVBREw7QUFFRSxhQUFPLEVBQUMsV0FGVjtBQUdFLGVBQVMsRUFBRW9CLGlEQUFJLENBQUNQLE9BQU8sQ0FBQ2hCLE1BQVQsRUFBaUJnQixPQUFPLENBQUNOLGFBQXpCLENBSGpCO0FBSUUsYUFBTyxFQUFFLE1BQU1TLFdBQVcsQ0FBQyxlQUFELENBSjVCO0FBQUEsOEJBTUUscUVBQUMsdURBQUQ7QUFBVSxpQkFBUyxFQUFFSCxPQUFPLENBQUNiO0FBQTdCO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBTkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZGLGVBb0JFLHFFQUFDLHlEQUFEO0FBQVMsZUFBUyxFQUFFYSxPQUFPLENBQUNMO0FBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBcEJGLGVBcUJFLHFFQUFDLDREQUFEO0FBQVksYUFBTyxFQUFDLFdBQXBCO0FBQWdDLGVBQVMsRUFBRUssT0FBTyxDQUFDSixJQUFuRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFyQkYsZUF3QkUscUVBQUMsc0RBQUQ7QUFBTSxlQUFTLE1BQWY7QUFBZ0IsYUFBTyxFQUFDLGNBQXhCO0FBQXVDLGFBQU8sRUFBRSxDQUFoRDtBQUFBLDhCQUNFLHFFQUFDLDREQUFEO0FBQ0UsVUFBRSxFQUFDLGlCQURMO0FBRUUsZUFBTyxFQUFFLE1BQU1PLFdBQVcsQ0FBQyxpQkFBRCxDQUY1QjtBQUFBLCtCQUlFLHFFQUFDLHlEQUFEO0FBQVksbUJBQVMsRUFBRUgsT0FBTyxDQUFDYjtBQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQU9FLHFFQUFDLDREQUFEO0FBQ0UsVUFBRSxFQUFDLGdCQURMO0FBRUUsaUJBQVMsRUFBRWEsT0FBTyxDQUFDaEIsTUFGckI7QUFHRSxlQUFPLEVBQUUsTUFBTW1CLFdBQVcsQ0FBQyxnQkFBRCxDQUg1QjtBQUFBLCtCQUtFLHFFQUFDLHdEQUFEO0FBQVcsbUJBQVMsRUFBRUgsT0FBTyxDQUFDYjtBQUE5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFQRixlQWVFLHFFQUFDLDREQUFEO0FBQ0UsVUFBRSxFQUFDLGNBREw7QUFFRSxlQUFPLEVBQUUsTUFBTWdCLFdBQVcsQ0FBQyxjQUFELENBRjVCO0FBQUEsK0JBSUUscUVBQUMsc0RBQUQ7QUFBUyxtQkFBUyxFQUFFSCxPQUFPLENBQUNiO0FBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQWZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkF4QkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFpREQsQ0FsRUQ7O0FBb0VlVyxvRUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzSUE7QUFDQTtBQVNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQSxNQUFNdkIsUUFBUSxHQUFHQyxvRUFBVSxDQUFFQyxLQUFELEtBQVk7QUFDdENDLFdBQVMsRUFBRTtBQUNUQyxTQUFLLEVBQUUsTUFERTtBQUVUQyxVQUFNLEVBQUUsWUFGQztBQUdUQyxXQUFPLEVBQUVKLEtBQUssQ0FBQ1EsT0FBTixDQUFjLENBQWQsQ0FIQTtBQUlUSCxhQUFTLEVBQUVMLEtBQUssQ0FBQ00sT0FBTixDQUFjLENBQWQsQ0FKRjtBQUtUYyxhQUFTLEVBQUU7QUFMRixHQUQyQjtBQVN0Q1csUUFBTSxFQUFFO0FBQ05DLFdBQU8sRUFBRSxNQURIO0FBRU5DLGlCQUFhLEVBQUUsUUFGVDtBQUdOQyxjQUFVLEVBQUU7QUFITixHQVQ4QjtBQWN0Q0MsUUFBTSxFQUFFO0FBQ05qQyxTQUFLLEVBQUVGLEtBQUssQ0FBQ1EsT0FBTixDQUFjLEVBQWQsQ0FERDtBQUVONEIsVUFBTSxFQUFFcEMsS0FBSyxDQUFDUSxPQUFOLENBQWMsRUFBZCxDQUZGO0FBR05ILGFBQVMsRUFBRUwsS0FBSyxDQUFDTSxPQUFOLENBQWMsQ0FBZDtBQUhMLEdBZDhCO0FBbUJ0QytCLFVBQVEsRUFBRTtBQUNSbEMsVUFBTSxFQUFHLEdBQUVILEtBQUssQ0FBQ1EsT0FBTixDQUFjLENBQWQsQ0FBaUI7QUFEcEI7QUFuQjRCLENBQVosQ0FBRCxDQUEzQjs7QUF3QkEsTUFBTThCLGFBQWEsR0FBSWhCLE1BQUQsSUFBWTtBQUNoQyxRQUFNQyxPQUFPLEdBQUd6QixRQUFRLEVBQXhCO0FBQ0EsUUFBTTBCLElBQUksR0FBR0MseURBQU8sRUFBcEI7QUFFQSxzQkFDRSxxRUFBQyxzREFBRDtBQUNFLGFBQVMsTUFEWDtBQUVFLGFBQVMsRUFBQyxRQUZaO0FBR0UsZ0JBQVksRUFBQyxRQUhmO0FBSUUsV0FBTyxFQUFFLENBSlg7QUFLRSxhQUFTLEVBQUVGLE9BQU8sQ0FBQ3RCLFNBTHJCO0FBQUEsNEJBT0UscUVBQUMsc0RBQUQ7QUFBTSxVQUFJLE1BQVY7QUFBVyxlQUFTLEVBQUVzQixPQUFPLENBQUNRLE1BQTlCO0FBQUEsOEJBQ0UscUVBQUMsd0RBQUQ7QUFBUSxpQkFBUyxFQUFFUixPQUFPLENBQUNZLE1BQTNCO0FBQW1DLFdBQUcsRUFBRVgsSUFBSSxDQUFDZSxJQUFMLENBQVVDO0FBQWxEO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRSxxRUFBQyw0REFBRDtBQUFZLGlCQUFTLEVBQUVqQixPQUFPLENBQUNjLFFBQS9CO0FBQXlDLGVBQU8sRUFBQyxJQUFqRDtBQUFzRCxhQUFLLEVBQUMsU0FBNUQ7QUFBQSxrQkFDR2IsSUFBSSxDQUFDZSxJQUFMLENBQVVFO0FBRGI7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBUEYsZUFjRSxxRUFBQyx3REFBRDtBQUFRLGFBQU8sRUFBQyxVQUFoQjtBQUEyQixXQUFLLEVBQUMsU0FBakM7QUFBMkMsYUFBTyxFQUFFLE1BQU1qQixJQUFJLENBQUNrQixPQUFMLEVBQTFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQWRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBb0JELENBeEJEOztBQTBCZUosNEVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcEVBO0FBQ0E7QUFFQTtBQUNBO0FBRUEsTUFBTUssV0FBVyxnQkFBR0MsMkRBQWEsRUFBakM7QUFFTyxTQUFTQyxZQUFULENBQXNCO0FBQUVDO0FBQUYsQ0FBdEIsRUFBb0M7QUFDekMsUUFBTXRCLElBQUksR0FBR3VCLGNBQWMsRUFBM0I7QUFDQSxzQkFBTyxxRUFBQyxXQUFELENBQWEsUUFBYjtBQUFzQixTQUFLLEVBQUV2QixJQUE3QjtBQUFBLGNBQW9Dc0I7QUFBcEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUFQO0FBQ0Q7QUFFTSxNQUFNckIsT0FBTyxHQUFHLE1BQU07QUFDM0IsU0FBT3VCLHdEQUFVLENBQUNMLFdBQUQsQ0FBakI7QUFDRCxDQUZNOztBQUlQLFNBQVNJLGNBQVQsR0FBMEI7QUFDeEIsUUFBTTtBQUFBLE9BQUNSLElBQUQ7QUFBQSxPQUFPVTtBQUFQLE1BQWtCQyxzREFBUSxDQUFDLElBQUQsQ0FBaEM7O0FBRUEsUUFBTUMsVUFBVSxHQUFHLE1BQU9DLE9BQVAsSUFBbUI7QUFDcENDLFdBQU8sQ0FBQ0MsR0FBUixDQUFZRixPQUFaOztBQUNBLFFBQUlBLE9BQUosRUFBYTtBQUNYLDBCQUEyQ0csVUFBVSxDQUFDSCxPQUFELENBQXJEO0FBQUEsWUFBTTtBQUFFSTtBQUFGLE9BQU47QUFBQSxZQUFrQkMsb0JBQWxCOztBQUNBQyxzREFBTSxDQUFDQyxHQUFQLENBQVcsYUFBWCxFQUEwQixJQUExQixFQUFnQztBQUFFQyxlQUFPLEVBQUU7QUFBWCxPQUFoQztBQUNBLFlBQU1DLDBEQUFVLENBQUNKLG9CQUFvQixDQUFDSyxHQUF0QixFQUEyQkwsb0JBQTNCLENBQWhCO0FBQ0FSLGFBQU8saUNBQU1RLG9CQUFOO0FBQTRCRCxhQUFLLEVBQUVBO0FBQW5DLFNBQVA7QUFFQSxhQUFPakIsSUFBUDtBQUNELEtBUEQsTUFPTztBQUNMbUIsc0RBQU0sQ0FBQ0ssTUFBUCxDQUFjLGFBQWQ7QUFDQWQsYUFBTyxDQUFDLEtBQUQsQ0FBUDtBQUNBLGFBQU8sS0FBUDtBQUNEO0FBQ0YsR0FkRDs7QUFnQkEsUUFBTXJCLGdCQUFnQixHQUFHLE9BQU9vQyxLQUFQLEVBQWNDLFFBQWQsS0FBMkI7QUFDbEQsV0FBTyxNQUFNQyxxREFBUSxDQUNsQjFDLElBRFUsR0FFVjJDLGVBRlUsQ0FFTSxJQUFJRCxxREFBUSxDQUFDMUMsSUFBVCxDQUFjNEMsa0JBQWxCLEVBRk4sRUFHVkMsSUFIVSxDQUdKQyxRQUFELElBQWM7QUFDbEJuQixnQkFBVSxDQUFDbUIsUUFBUSxDQUFDL0IsSUFBVixDQUFWO0FBQ0QsS0FMVSxDQUFiO0FBTUQsR0FQRDs7QUFTQSxRQUFNVixnQkFBZ0IsR0FBRyxPQUFPbUMsS0FBUCxFQUFjQyxRQUFkLEtBQTJCO0FBQ2xELFdBQU8sTUFBTUMscURBQVEsQ0FDbEIxQyxJQURVLEdBRVYyQyxlQUZVLENBRU0sSUFBSUQscURBQVEsQ0FBQzFDLElBQVQsQ0FBYytDLGtCQUFsQixFQUZOLEVBR1ZGLElBSFUsQ0FHSkMsUUFBRCxJQUFjO0FBQ2xCbkIsZ0JBQVUsQ0FBQ21CLFFBQVEsQ0FBQy9CLElBQVYsQ0FBVjtBQUNELEtBTFUsQ0FBYjtBQU1ELEdBUEQ7O0FBU0EsUUFBTUcsT0FBTyxHQUFHLFlBQVk7QUFDMUIsV0FBTyxNQUFNd0IscURBQVEsQ0FDbEIxQyxJQURVLEdBRVZnRCxPQUZVLEdBR1ZILElBSFUsQ0FHTCxNQUFNO0FBQ1ZsQixnQkFBVSxDQUFDLEtBQUQsQ0FBVjtBQUNELEtBTFUsQ0FBYjtBQU1ELEdBUEQ7O0FBU0FzQix5REFBUyxDQUFDLE1BQU07QUFDZCxVQUFNQyxXQUFXLEdBQUdSLHFEQUFRLENBQUMxQyxJQUFULEdBQWdCbUQsa0JBQWhCLENBQW1DeEIsVUFBbkMsQ0FBcEI7QUFFQSxXQUFPLE1BQU11QixXQUFXLEVBQXhCO0FBQ0QsR0FKUSxFQUlOLEVBSk0sQ0FBVDtBQU1BLFNBQU87QUFDTG5DLFFBREs7QUFFTFgsb0JBRks7QUFHTEMsb0JBSEs7QUFJTGE7QUFKSyxHQUFQO0FBTUQ7O0FBRUQsTUFBTWEsVUFBVSxHQUFJaEIsSUFBRCxJQUFVO0FBQzNCLFNBQU87QUFDTHVCLE9BQUcsRUFBRXZCLElBQUksQ0FBQ3VCLEdBREw7QUFFTEUsU0FBSyxFQUFFekIsSUFBSSxDQUFDeUIsS0FGUDtBQUdMdkIsUUFBSSxFQUFFRixJQUFJLENBQUNxQyxXQUhOO0FBSUx2QyxZQUFRLEVBQUVFLElBQUksQ0FBQ3FDLFdBQUwsQ0FBaUJDLE1BQWpCLENBQXdCLENBQXhCLEVBQTJCLENBQTNCLElBQWdDdEMsSUFBSSxDQUFDdUIsR0FBTCxDQUFTZSxNQUFULENBQWdCLENBQWhCLEVBQW1CLENBQW5CLENBSnJDO0FBS0xyQixTQUFLLEVBQUVqQixJQUFJLENBQUN1QyxFQUxQO0FBTUxDLFlBQVEsRUFBRXhDLElBQUksQ0FBQ3lDLFlBQUwsQ0FBa0IsQ0FBbEIsRUFBcUJDLFVBTjFCO0FBT0x6QyxVQUFNLEVBQUVELElBQUksQ0FBQ3lDLFlBQUwsQ0FBa0IsQ0FBbEIsRUFBcUJFO0FBUHhCLEdBQVA7QUFTRCxDQVZELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3RUE7QUFFQSxNQUFNQyxTQUFTLEdBQUdqQixxREFBUSxDQUFDaUIsU0FBVCxFQUFsQjtBQUVPLE1BQU10QixVQUFVLEdBQUcsT0FBT0MsR0FBUCxFQUFZc0IsSUFBWixLQUFxQjtBQUM3QyxTQUFPRCxTQUFTLENBQ2JFLFVBREksQ0FDTyxPQURQLEVBRUpDLEdBRkksQ0FFQXhCLEdBRkEsRUFHSkgsR0FISSxtQkFHS3lCLElBSEwsR0FHYTtBQUFFRyxTQUFLLEVBQUU7QUFBVCxHQUhiLENBQVA7QUFJRCxDQUxNO0FBT0EsTUFBTUMsV0FBVyxHQUFHLFlBQVk7QUFDckMsUUFBTUMsUUFBUSxHQUFHLE1BQU1OLFNBQVMsQ0FBQ0UsVUFBVixDQUFxQixPQUFyQixFQUE4QkssR0FBOUIsRUFBdkI7QUFDQSxNQUFJcEIsUUFBUSxHQUFHLEVBQWY7QUFDQW1CLFVBQVEsQ0FBQ0UsT0FBVCxDQUFrQkwsR0FBRCxJQUFTO0FBQ3hCaEIsWUFBUSxDQUFDZ0IsR0FBRyxDQUFDTSxFQUFMLENBQVIscUJBQ0tOLEdBQUcsQ0FBQ0YsSUFBSixFQURMO0FBR0QsR0FKRDtBQUtBL0IsU0FBTyxDQUFDQyxHQUFSLENBQVlnQixRQUFaO0FBQ0EsU0FBT0EsUUFBUDtBQUNELENBVk0sQzs7Ozs7Ozs7Ozs7O0FDWFA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7O0FBRUEsSUFBSSxDQUFDSixtREFBQSxDQUFpQjJCLElBQWpCLENBQXNCQyxNQUEzQixFQUFtQztBQUNqQzVCLHFEQUFBLENBQWlCNkIsYUFBakIsQ0FBK0I7QUFDN0JDLFVBQU0sRUFBRUMseUNBRHFCO0FBRTdCQyxjQUFVLEVBQUVELG9DQUZpQjtBQUc3QkUsYUFBUyxFQUFFRixvQkFBMkNHO0FBSHpCLEdBQS9CO0FBS0Q7O0FBRWNsQyxrSEFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNaQTtBQUNBO0NBR0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTXBFLFFBQVEsR0FBR0Msb0VBQVUsQ0FBRUMsS0FBRCxLQUFZO0FBQ3RDQyxXQUFTLEVBQUU7QUFDVEMsU0FBSyxFQUFFLE9BREU7QUFFVGtDLFVBQU0sRUFBRTtBQUZDO0FBRDJCLENBQVosQ0FBRCxDQUEzQjs7QUFPQSxNQUFNaUUsU0FBUyxHQUFHLE1BQU07QUFDdEIsUUFBTTlFLE9BQU8sR0FBR3pCLFFBQVEsRUFBeEI7QUFDQSxRQUFNMEIsSUFBSSxHQUFHQyx5REFBTyxFQUFwQjtBQUNBLHNCQUNFO0FBQUEsNEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSw2QkFDRTtBQUNFLCtCQUF1QixFQUFFO0FBQ3ZCNkUsZ0JBQU0sRUFBRztBQUNyQjtBQUNBO0FBSG1DO0FBRDNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGLEVBVUc5RSxJQUFJLFNBQUosSUFBQUEsSUFBSSxXQUFKLElBQUFBLElBQUksQ0FBRWUsSUFBTixnQkFDQztBQUFLLGVBQVMsRUFBRWhCLE9BQU8sQ0FBQ3RCLFNBQXhCO0FBQUEsNkJBQ0UscUVBQUMsaUVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREQsZ0JBS0M7QUFBSyxlQUFTLEVBQUVzQixPQUFPLENBQUN0QixTQUF4QjtBQUFBLDZCQUNFLHFFQUFDLHlEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFmSjtBQUFBLGtCQURGO0FBc0JELENBekJEOztBQTJCZW9HLHdFQUFmLEU7Ozs7Ozs7Ozs7O0FDMUNBLDhDOzs7Ozs7Ozs7OztBQ0FBLHFEOzs7Ozs7Ozs7OztBQ0FBLHVDOzs7Ozs7Ozs7OztBQ0FBLHlDOzs7Ozs7Ozs7OztBQ0FBLDBDOzs7Ozs7Ozs7OztBQ0FBLCtDOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDJDOzs7Ozs7Ozs7OztBQ0FBLDJDOzs7Ozs7Ozs7OztBQ0FBLDJDOzs7Ozs7Ozs7OztBQ0FBLGtEIiwiZmlsZSI6InBhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCBjbHN4IGZyb20gXCJjbGFzc25hbWVzXCI7XHJcbmltcG9ydCB7XHJcbiAgQnV0dG9uLFxyXG4gIERpdmlkZXIsXHJcbiAgR3JpZCxcclxuICBJY29uQnV0dG9uLFxyXG4gIG1ha2VTdHlsZXMsXHJcbiAgVHlwb2dyYXBoeSxcclxufSBmcm9tIFwiQG1hdGVyaWFsLXVpL2NvcmVcIjtcclxuaW1wb3J0IHsgZ3JleSB9IGZyb20gXCJAbWF0ZXJpYWwtdWkvY29yZS9jb2xvcnNcIjtcclxuXHJcbmltcG9ydCB7IEZpRmFjZWJvb2ssIEZpVHdpdHRlciwgRmlHaXRodWIgfSBmcm9tIFwicmVhY3QtaWNvbnMvZmlcIjtcclxuaW1wb3J0IHsgRmNHb29nbGUgfSBmcm9tIFwicmVhY3QtaWNvbnMvZmNcIjtcclxuaW1wb3J0IHsgRmFBcHBsZSB9IGZyb20gXCJyZWFjdC1pY29ucy9mYVwiO1xyXG5cclxuLy8gQ29tcG9uZW50c1xyXG5pbXBvcnQgeyB1c2VBdXRoIH0gZnJvbSBcIkBsaWIvYXV0aFwiO1xyXG5cclxuY29uc3QgdXNlU3R5bGUgPSBtYWtlU3R5bGVzKCh0aGVtZSkgPT4gKHtcclxuICBjb250YWluZXI6IHtcclxuICAgIHdpZHRoOiBcIjkwdndcIixcclxuICAgIG1hcmdpbjogXCI0MCUgYXV0b1wiLFxyXG4gICAgcGFkZGluZzogXCIzcmVtXCIsXHJcbiAgICBib3hTaGFkb3c6IHRoZW1lLnNoYWRvd3NbOF0sXHJcbiAgfSxcclxuXHJcbiAgYnV0dG9uOiB7XHJcbiAgICBtYXJnaW46IFwiMS41cmVtIDByZW1cIixcclxuICAgIHBhZGRpbmc6IGAke3RoZW1lLnNwYWNpbmcoMil9cHggJHt0aGVtZS5zcGFjaW5nKDApfXB4YCxcclxuICAgIGxldHRlclNwYWNpbmc6IFwiM3B4XCIsXHJcbiAgICAvLyBiYWNrZ3JvdW5kQ29sb3I6IFwiaW5oZXJpdFwiLFxyXG4gIH0sXHJcblxyXG4gIGljb246IHtcclxuICAgIGZvbnRTaXplOiB0aGVtZS5zcGFjaW5nKDMpLFxyXG4gICAgbWFyZ2luOiBgMHJlbSAke3RoZW1lLnNwYWNpbmcoMSl9cHhgLFxyXG4gIH0sXHJcblxyXG4gIGJ1dHRvbl9naXRodWI6IHtcclxuICAgIGJhY2tncm91bmRDb2xvcjogZ3JleVs5MDBdLFxyXG4gICAgY29sb3I6IFwid2hpdGVcIixcclxuXHJcbiAgICBcIiY6YWN0aXZlXCI6IHtcclxuICAgICAgYmFja2dyb3VuZENvbG9yOiBncmV5WzgwMF0sXHJcbiAgICAgIHRyYW5zZm9ybTogXCJzY2FsZSgwLjk1KVwiLFxyXG4gICAgfSxcclxuICAgIFwiJjpob3ZlclwiOiB7XHJcbiAgICAgIGJhY2tncm91bmRDb2xvcjogZ3JleVs4MDBdLFxyXG4gICAgfSxcclxuICB9LFxyXG5cclxuICBidXR0b25fZ29vZ2xlOiB7XHJcbiAgICBiYWNrZ3JvdW5kQ29sb3I6IGdyZXlbMTAwXSxcclxuICAgIGNvbG9yOiBncmV5WzcwMF0sXHJcbiAgICBcIiY6YWN0aXZlXCI6IHtcclxuICAgICAgYmFja2dyb3VuZENvbG9yOiBncmV5WzIwMF0sXHJcbiAgICAgIHRyYW5zZm9ybTogXCJzY2FsZSgwLjk1KVwiLFxyXG4gICAgfSxcclxuICAgIFwiJjpob3ZlclwiOiB7XHJcbiAgICAgIGJhY2tncm91bmRDb2xvcjogZ3JleVsyMDBdLFxyXG4gICAgfSxcclxuICB9LFxyXG5cclxuICBkaXZpZGVyOiB7XHJcbiAgICBtYXJnaW46IFwiMXJlbSAwcmVtXCIsXHJcbiAgfSxcclxuICBtb3JlOiB7XHJcbiAgICB0ZXh0QWxpZ246IFwiY2VudGVyXCIsXHJcbiAgfSxcclxufSkpO1xyXG5cclxuY29uc3QgTG9naW4gPSAocGFyYW1zKSA9PiB7XHJcbiAgY29uc3QgY2xhc3NlcyA9IHVzZVN0eWxlKCk7XHJcbiAgY29uc3QgYXV0aCA9IHVzZUF1dGgoKTtcclxuXHJcbiAgY29uc3QgaGFuZGxlTG9naW4gPSAodGFyZ2V0KSA9PiB7XHJcbiAgICBpZiAodGFyZ2V0ID09PSBcImdpdGh1Yl9idXR0b25cIikge1xyXG4gICAgICBhdXRoLnNpZ25pbldpdGhHaXRodWIoKTtcclxuICAgIH0gZWxzZSBpZiAodGFyZ2V0ID09PSBcImdvb2dsZV9idXR0b25cIikge1xyXG4gICAgICBhdXRoLnNpZ25pbldpdGhHb29nbGUoKTtcclxuICAgIH0gZWxzZSBpZiAodGFyZ2V0ID09PSBcInR3aXR0ZXJfYnV0dG9uXCIpIHtcclxuICAgICAgLy8gICBjb25zb2xlLmxvZyhcIlRXSVRURVJcIik7XHJcbiAgICB9IGVsc2UgaWYgKHRhcmdldCA9PT0gXCJmYWNlYm9va19idXR0b25cIikge1xyXG4gICAgICAvLyAgIGNvbnNvbGUubG9nKFwiRkFDRUJPT0tcIik7XHJcbiAgICB9IGVsc2UgaWYgKHRhcmdldCA9PT0gXCJhcHBsZV9idXR0b25cIikge1xyXG4gICAgICAvLyAgIGNvbnNvbGUubG9nKFwiQVBQTEVcIik7XHJcbiAgICB9XHJcbiAgfTtcclxuICByZXR1cm4gKFxyXG4gICAgPEdyaWQgY29udGFpbmVyIGRpcmVjdGlvbj0nY29sdW1uJyBjbGFzc05hbWU9e2NsYXNzZXMuY29udGFpbmVyfT5cclxuICAgICAgPEJ1dHRvblxyXG4gICAgICAgIGlkPSdnaXRodWJfYnV0dG9uJ1xyXG4gICAgICAgIHZhcmlhbnQ9J2NvbnRhaW5lZCdcclxuICAgICAgICBjbGFzc05hbWU9e2Nsc3goY2xhc3Nlcy5idXR0b24sIGNsYXNzZXMuYnV0dG9uX2dpdGh1Yil9XHJcbiAgICAgICAgb25DbGljaz17KCkgPT4gaGFuZGxlTG9naW4oXCJnaXRodWJfYnV0dG9uXCIpfVxyXG4gICAgICA+XHJcbiAgICAgICAgPEZpR2l0aHViIGNsYXNzTmFtZT17Y2xhc3Nlcy5pY29ufSAvPlxyXG4gICAgICAgIFNpZ24gaW4gd2l0aCBHaXRodWJcclxuICAgICAgPC9CdXR0b24+XHJcbiAgICAgIDxCdXR0b25cclxuICAgICAgICBpZD0nZ29vZ2xlX2J1dHRvbidcclxuICAgICAgICB2YXJpYW50PSdjb250YWluZWQnXHJcbiAgICAgICAgY2xhc3NOYW1lPXtjbHN4KGNsYXNzZXMuYnV0dG9uLCBjbGFzc2VzLmJ1dHRvbl9nb29nbGUpfVxyXG4gICAgICAgIG9uQ2xpY2s9eygpID0+IGhhbmRsZUxvZ2luKFwiZ29vZ2xlX2J1dHRvblwiKX1cclxuICAgICAgPlxyXG4gICAgICAgIDxGY0dvb2dsZSBjbGFzc05hbWU9e2NsYXNzZXMuaWNvbn0gLz5cclxuICAgICAgICBTaWduIGluIHdpdGggR29vZ2xlXHJcbiAgICAgIDwvQnV0dG9uPlxyXG5cclxuICAgICAgPERpdmlkZXIgY2xhc3NOYW1lPXtjbGFzc2VzLmRpdmlkZXJ9IC8+XHJcbiAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9J3N1YnRpdGxlMicgY2xhc3NOYW1lPXtjbGFzc2VzLm1vcmV9PlxyXG4gICAgICAgIE90aGVyc1xyXG4gICAgICA8L1R5cG9ncmFwaHk+XHJcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PSdzcGFjZS1hcm91bmQnIHNwYWNpbmc9ezJ9PlxyXG4gICAgICAgIDxJY29uQnV0dG9uXHJcbiAgICAgICAgICBpZD0nZmFjZWJvb2tfYnV0dG9uJ1xyXG4gICAgICAgICAgb25DbGljaz17KCkgPT4gaGFuZGxlTG9naW4oXCJmYWNlYm9va19idXR0b25cIil9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPEZpRmFjZWJvb2sgY2xhc3NOYW1lPXtjbGFzc2VzLmljb259IC8+XHJcbiAgICAgICAgPC9JY29uQnV0dG9uPlxyXG4gICAgICAgIDxJY29uQnV0dG9uXHJcbiAgICAgICAgICBpZD0ndHdpdHRlcl9idXR0b24nXHJcbiAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuYnV0dG9ufVxyXG4gICAgICAgICAgb25DbGljaz17KCkgPT4gaGFuZGxlTG9naW4oXCJ0d2l0dGVyX2J1dHRvblwiKX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICA8RmlUd2l0dGVyIGNsYXNzTmFtZT17Y2xhc3Nlcy5pY29ufSAvPlxyXG4gICAgICAgIDwvSWNvbkJ1dHRvbj5cclxuXHJcbiAgICAgICAgPEljb25CdXR0b25cclxuICAgICAgICAgIGlkPSdhcHBsZV9idXR0b24nXHJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBoYW5kbGVMb2dpbihcImFwcGxlX2J1dHRvblwiKX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICA8RmFBcHBsZSBjbGFzc05hbWU9e2NsYXNzZXMuaWNvbn0gLz5cclxuICAgICAgICA8L0ljb25CdXR0b24+XHJcbiAgICAgIDwvR3JpZD5cclxuICAgIDwvR3JpZD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgTG9naW47XHJcbiIsImltcG9ydCBjbHN4IGZyb20gXCJjbGFzc25hbWVzXCI7XHJcbmltcG9ydCB7XHJcbiAgQXZhdGFyLFxyXG4gIEJ1dHRvbixcclxuICBEaXZpZGVyLFxyXG4gIEdyaWQsXHJcbiAgSWNvbkJ1dHRvbixcclxuICBtYWtlU3R5bGVzLFxyXG4gIFR5cG9ncmFwaHksXHJcbn0gZnJvbSBcIkBtYXRlcmlhbC11aS9jb3JlXCI7XHJcbmltcG9ydCB7IGdyZXkgfSBmcm9tIFwiQG1hdGVyaWFsLXVpL2NvcmUvY29sb3JzXCI7XHJcblxyXG5pbXBvcnQgeyBGaUZhY2Vib29rLCBGaVR3aXR0ZXIsIEZpR2l0aHViIH0gZnJvbSBcInJlYWN0LWljb25zL2ZpXCI7XHJcbmltcG9ydCB7IEZjR29vZ2xlIH0gZnJvbSBcInJlYWN0LWljb25zL2ZjXCI7XHJcbmltcG9ydCB7IEZhQXBwbGUgfSBmcm9tIFwicmVhY3QtaWNvbnMvZmFcIjtcclxuXHJcbmltcG9ydCB7IHVzZUF1dGggfSBmcm9tIFwiQGxpYi9hdXRoXCI7XHJcblxyXG5jb25zdCB1c2VTdHlsZSA9IG1ha2VTdHlsZXMoKHRoZW1lKSA9PiAoe1xyXG4gIGNvbnRhaW5lcjoge1xyXG4gICAgd2lkdGg6IFwiOTB2d1wiLFxyXG4gICAgbWFyZ2luOiBcIjEwcmVtIGF1dG9cIixcclxuICAgIHBhZGRpbmc6IHRoZW1lLnNwYWNpbmcoMiksXHJcbiAgICBib3hTaGFkb3c6IHRoZW1lLnNoYWRvd3NbOF0sXHJcbiAgICB0ZXh0QWxpZ246IFwiY2VudGVyXCIsXHJcbiAgfSxcclxuXHJcbiAgaGVhZGVyOiB7XHJcbiAgICBkaXNwbGF5OiBcImZsZXhcIixcclxuICAgIGZsZXhEaXJlY3Rpb246IFwiY29sdW1uXCIsXHJcbiAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxyXG4gIH0sXHJcbiAgYXZhdGFyOiB7XHJcbiAgICB3aWR0aDogdGhlbWUuc3BhY2luZygxNSksXHJcbiAgICBoZWlnaHQ6IHRoZW1lLnNwYWNpbmcoMTUpLFxyXG4gICAgYm94U2hhZG93OiB0aGVtZS5zaGFkb3dzWzhdLFxyXG4gIH0sXHJcbiAgdXNlcm5hbWU6IHtcclxuICAgIG1hcmdpbjogYCR7dGhlbWUuc3BhY2luZygxKX1weCAwcHhgLFxyXG4gIH0sXHJcbn0pKTtcclxuXHJcbmNvbnN0IHdlbGNvbWVTY3JlZW4gPSAocGFyYW1zKSA9PiB7XHJcbiAgY29uc3QgY2xhc3NlcyA9IHVzZVN0eWxlKCk7XHJcbiAgY29uc3QgYXV0aCA9IHVzZUF1dGgoKTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxHcmlkXHJcbiAgICAgIGNvbnRhaW5lclxyXG4gICAgICBkaXJlY3Rpb249J2NvbHVtbidcclxuICAgICAgYWxpZ25Db250ZW50PSdjZW50ZXInXHJcbiAgICAgIHNwYWNpbmc9ezN9XHJcbiAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250YWluZXJ9XHJcbiAgICA+XHJcbiAgICAgIDxHcmlkIGl0ZW0gY2xhc3NOYW1lPXtjbGFzc2VzLmhlYWRlcn0+XHJcbiAgICAgICAgPEF2YXRhciBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfSBzcmM9e2F1dGgudXNlci5pbWdVcmx9IC8+XHJcbiAgICAgICAgPFR5cG9ncmFwaHkgY2xhc3NOYW1lPXtjbGFzc2VzLnVzZXJuYW1lfSB2YXJpYW50PSdoNScgY29sb3I9J3ByaW1hcnknPlxyXG4gICAgICAgICAge2F1dGgudXNlci5uYW1lfVxyXG4gICAgICAgIDwvVHlwb2dyYXBoeT5cclxuICAgICAgPC9HcmlkPlxyXG5cclxuICAgICAgPEJ1dHRvbiB2YXJpYW50PSdvdXRsaW5lZCcgY29sb3I9J3ByaW1hcnknIG9uQ2xpY2s9eygpID0+IGF1dGguc2lnbm91dCgpfT5cclxuICAgICAgICBMb2dvdXRcclxuICAgICAgPC9CdXR0b24+XHJcbiAgICA8L0dyaWQ+XHJcbiAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHdlbGNvbWVTY3JlZW47XHJcbiIsImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0LCB1c2VDb250ZXh0LCBjcmVhdGVDb250ZXh0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBjb29raWUgZnJvbSBcImpzLWNvb2tpZVwiO1xyXG5cclxuaW1wb3J0IHsgY3JlYXRlVXNlciB9IGZyb20gXCJAbGliL2RiXCI7XHJcbmltcG9ydCBmaXJlYmFzZSBmcm9tIFwiQGxpYi9maXJlYmFzZVwiO1xyXG5cclxuY29uc3QgYXV0aENvbnRleHQgPSBjcmVhdGVDb250ZXh0KCk7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gQXV0aFByb3ZpZGVyKHsgY2hpbGRyZW4gfSkge1xyXG4gIGNvbnN0IGF1dGggPSB1c2VQcm92aWRlQXV0aCgpO1xyXG4gIHJldHVybiA8YXV0aENvbnRleHQuUHJvdmlkZXIgdmFsdWU9e2F1dGh9PntjaGlsZHJlbn08L2F1dGhDb250ZXh0LlByb3ZpZGVyPjtcclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IHVzZUF1dGggPSAoKSA9PiB7XHJcbiAgcmV0dXJuIHVzZUNvbnRleHQoYXV0aENvbnRleHQpO1xyXG59O1xyXG5cclxuZnVuY3Rpb24gdXNlUHJvdmlkZUF1dGgoKSB7XHJcbiAgY29uc3QgW3VzZXIsIHNldFVzZXJdID0gdXNlU3RhdGUobnVsbCk7XHJcblxyXG4gIGNvbnN0IGhhbmRsZVVzZXIgPSBhc3luYyAocmF3VXNlcikgPT4ge1xyXG4gICAgY29uc29sZS5sb2cocmF3VXNlcik7XHJcbiAgICBpZiAocmF3VXNlcikge1xyXG4gICAgICBjb25zdCB7IHRva2VuLCAuLi51c2VyRGF0YVdpdGhvdXRUb2tlbiB9ID0gZm9ybWF0VXNlcihyYXdVc2VyKTtcclxuICAgICAgY29va2llLnNldChcInZsYW1lci1hdXRoXCIsIHRydWUsIHsgZXhwaXJlczogMSB9KTtcclxuICAgICAgYXdhaXQgY3JlYXRlVXNlcih1c2VyRGF0YVdpdGhvdXRUb2tlbi51aWQsIHVzZXJEYXRhV2l0aG91dFRva2VuKTtcclxuICAgICAgc2V0VXNlcih7IC4uLnVzZXJEYXRhV2l0aG91dFRva2VuLCB0b2tlbjogdG9rZW4gfSk7XHJcblxyXG4gICAgICByZXR1cm4gdXNlcjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvb2tpZS5yZW1vdmUoXCJ2bGFtZXItYXV0aFwiKTtcclxuICAgICAgc2V0VXNlcihmYWxzZSk7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBjb25zdCBzaWduaW5XaXRoR2l0aHViID0gYXN5bmMgKGVtYWlsLCBwYXNzd29yZCkgPT4ge1xyXG4gICAgcmV0dXJuIGF3YWl0IGZpcmViYXNlXHJcbiAgICAgIC5hdXRoKClcclxuICAgICAgLnNpZ25JbldpdGhQb3B1cChuZXcgZmlyZWJhc2UuYXV0aC5HaXRodWJBdXRoUHJvdmlkZXIoKSlcclxuICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgaGFuZGxlVXNlcihyZXNwb25zZS51c2VyKTtcclxuICAgICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgY29uc3Qgc2lnbmluV2l0aEdvb2dsZSA9IGFzeW5jIChlbWFpbCwgcGFzc3dvcmQpID0+IHtcclxuICAgIHJldHVybiBhd2FpdCBmaXJlYmFzZVxyXG4gICAgICAuYXV0aCgpXHJcbiAgICAgIC5zaWduSW5XaXRoUG9wdXAobmV3IGZpcmViYXNlLmF1dGguR29vZ2xlQXV0aFByb3ZpZGVyKCkpXHJcbiAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgIGhhbmRsZVVzZXIocmVzcG9uc2UudXNlcik7XHJcbiAgICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IHNpZ25vdXQgPSBhc3luYyAoKSA9PiB7XHJcbiAgICByZXR1cm4gYXdhaXQgZmlyZWJhc2VcclxuICAgICAgLmF1dGgoKVxyXG4gICAgICAuc2lnbk91dCgpXHJcbiAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICBoYW5kbGVVc2VyKGZhbHNlKTtcclxuICAgICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGNvbnN0IHVuc3Vic2NyaWJlID0gZmlyZWJhc2UuYXV0aCgpLm9uQXV0aFN0YXRlQ2hhbmdlZChoYW5kbGVVc2VyKTtcclxuXHJcbiAgICByZXR1cm4gKCkgPT4gdW5zdWJzY3JpYmUoKTtcclxuICB9LCBbXSk7XHJcblxyXG4gIHJldHVybiB7XHJcbiAgICB1c2VyLFxyXG4gICAgc2lnbmluV2l0aEdpdGh1YixcclxuICAgIHNpZ25pbldpdGhHb29nbGUsXHJcbiAgICBzaWdub3V0LFxyXG4gIH07XHJcbn1cclxuXHJcbmNvbnN0IGZvcm1hdFVzZXIgPSAodXNlcikgPT4ge1xyXG4gIHJldHVybiB7XHJcbiAgICB1aWQ6IHVzZXIudWlkLFxyXG4gICAgZW1haWw6IHVzZXIuZW1haWwsXHJcbiAgICBuYW1lOiB1c2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgdXNlcm5hbWU6IHVzZXIuZGlzcGxheU5hbWUuc3Vic3RyKDAsIDIpICsgdXNlci51aWQuc3Vic3RyKDAsIDQpLFxyXG4gICAgdG9rZW46IHVzZXIueWEsXHJcbiAgICBwcm92aWRlcjogdXNlci5wcm92aWRlckRhdGFbMF0ucHJvdmlkZXJJZCxcclxuICAgIGltZ1VybDogdXNlci5wcm92aWRlckRhdGFbMF0ucGhvdG9VUkwsXHJcbiAgfTtcclxufTtcclxuIiwiaW1wb3J0IGZpcmViYXNlIGZyb20gXCJAbGliL2ZpcmViYXNlXCI7XHJcblxyXG5jb25zdCBmaXJlc3RvcmUgPSBmaXJlYmFzZS5maXJlc3RvcmUoKTtcclxuXHJcbmV4cG9ydCBjb25zdCBjcmVhdGVVc2VyID0gYXN5bmMgKHVpZCwgZGF0YSkgPT4ge1xyXG4gIHJldHVybiBmaXJlc3RvcmVcclxuICAgIC5jb2xsZWN0aW9uKFwidXNlcnNcIilcclxuICAgIC5kb2ModWlkKVxyXG4gICAgLnNldCh7IC4uLmRhdGEgfSwgeyBtZXJnZTogdHJ1ZSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRBbGxVc2VycyA9IGFzeW5jICgpID0+IHtcclxuICBjb25zdCBzbmFwc2hvdCA9IGF3YWl0IGZpcmVzdG9yZS5jb2xsZWN0aW9uKFwidXNlcnNcIikuZ2V0KCk7XHJcbiAgbGV0IHJlc3BvbnNlID0ge307XHJcbiAgc25hcHNob3QuZm9yRWFjaCgoZG9jKSA9PiB7XHJcbiAgICByZXNwb25zZVtkb2MuaWRdID0ge1xyXG4gICAgICAuLi5kb2MuZGF0YSgpLFxyXG4gICAgfTtcclxuICB9KTtcclxuICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgcmV0dXJuIHJlc3BvbnNlO1xyXG59O1xyXG4iLCJpbXBvcnQgKiBhcyBmaXJlYmFzZSBmcm9tIFwiZmlyZWJhc2UvYXBwXCI7XHJcbmltcG9ydCBcImZpcmViYXNlL2F1dGhcIjtcclxuaW1wb3J0IFwiZmlyZWJhc2UvZmlyZXN0b3JlXCI7XHJcblxyXG5pZiAoIWZpcmViYXNlLmRlZmF1bHQuYXBwcy5sZW5ndGgpIHtcclxuICBmaXJlYmFzZS5kZWZhdWx0LmluaXRpYWxpemVBcHAoe1xyXG4gICAgYXBpS2V5OiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19GSVJFQkFTRV9BUElfS0VZLFxyXG4gICAgYXV0aERvbWFpbjogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfRklSRUJBU0VfQVVUSF9ET01BSU4sXHJcbiAgICBwcm9qZWN0SWQ6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0ZJUkVCQVNFX1BST0pFQ1RfSUQsXHJcbiAgfSk7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZpcmViYXNlLmRlZmF1bHQ7XHJcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgeyBtYWtlU3R5bGVzIH0gZnJvbSBcIkBtYXRlcmlhbC11aS9jb3JlXCI7XHJcblxyXG4vLyBDb21wb25lbnRzXHJcbmltcG9ydCB7IHVzZUF1dGggfSBmcm9tIFwiQGxpYi9hdXRoXCI7XHJcbmltcG9ydCBMb2dpbiBmcm9tIFwiQGNvbXBvbmVudHMvTG9naW5cIjtcclxuaW1wb3J0IFdlbGNvbWVTY3JlZW4gZnJvbSBcIkBjb21wb25lbnRzL1dlbGNvbWVTY3JlZW5cIjtcclxuY29uc3QgdXNlU3R5bGUgPSBtYWtlU3R5bGVzKCh0aGVtZSkgPT4gKHtcclxuICBjb250YWluZXI6IHtcclxuICAgIHdpZHRoOiBcIjEwMHZ3XCIsXHJcbiAgICBoZWlnaHQ6IFwiMTAwdmhcIixcclxuICB9LFxyXG59KSk7XHJcblxyXG5jb25zdCBEYXNoYm9hcmQgPSAoKSA9PiB7XHJcbiAgY29uc3QgY2xhc3NlcyA9IHVzZVN0eWxlKCk7XHJcbiAgY29uc3QgYXV0aCA9IHVzZUF1dGgoKTtcclxuICByZXR1cm4gKFxyXG4gICAgPD5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgPHNjcmlwdFxyXG4gICAgICAgICAgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3tcclxuICAgICAgICAgICAgX19odG1sOiBgaWYgKGRvY3VtZW50LmNvb2tpZSAmJiBkb2N1bWVudC5jb29raWUuaW5jbHVkZXMoJ3ZsYW1lci1hdXRoJykpIHtcclxuICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gXCIvZGFzaGJvYXJkXCJcclxuICAgICAgICB9YCxcclxuICAgICAgICAgIH19XHJcbiAgICAgICAgLz5cclxuICAgICAgPC9IZWFkPlxyXG4gICAgICB7YXV0aD8udXNlciA/IChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250YWluZXJ9PlxyXG4gICAgICAgICAgPFdlbGNvbWVTY3JlZW4+PC9XZWxjb21lU2NyZWVuPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICApIDogKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmNvbnRhaW5lcn0+XHJcbiAgICAgICAgICA8TG9naW4+SGVsbG8gVmxhbWVyczwvTG9naW4+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICl9XHJcbiAgICA8Lz5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgRGFzaGJvYXJkO1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9jb2xvcnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiY2xhc3NuYW1lc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJmaXJlYmFzZS9hcHBcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlyZWJhc2UvYXV0aFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJmaXJlYmFzZS9maXJlc3RvcmVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwianMtY29va2llXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvaGVhZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1pY29ucy9mYVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1pY29ucy9mY1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1pY29ucy9maVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==