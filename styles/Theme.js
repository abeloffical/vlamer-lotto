import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
  palette: {
    //   background: {
    //     paper: mode === "light" ? "#FFF" : "#000",
    //     default: mode === "light" ? "#FFF" : "#000",
    //     level2: mode === "light" ? "#F5f5f5" : "#050505",
    //     level1: mode === "light" ? "#FFF" : "#000",
    //   },
    type: "light",

    primary: {
      main: "#2c2925",
    },
    secondary: {
      main: "#73e8ff",
    },
    text: {
      primary: "rgba(0, 0, 0)",
      secondary: "rgba(255, 255, 255)",
    },
    divider: "#73e8ff",
  },

  typography: {
    h1: {
      fontFamily: "Coolvetica",
      fontSize: "3.5rem",
      fontWeight: 600,
      letterSpacing: "0.2rem",
    },
    h2: {
      fontFamily: "Coolvetica",
      fontSize: "3rem",
      fontWeight: 400,
      letterSpacing: "0.1rem",
    },
    h3: {
      fontFamily: "Montserrat",
      fontSize: "2.2rem",
      fontWeight: 300,
      letterSpacing: "0.1rem",
    },

    h5: {
      fontFamily: "Coolvetica",
      fontSize: "1.5rem",
      fontWeight: 300,
      letterSpacing: "0.1rem",
    },

    h6: {
      fontFamily: "Coolvetica",
      fontSize: "1.2rem",
      fontWeight: 300,
      letterSpacing: "0.1rem",
    },
    body1: {
      fontFamily: "Montserrat",
      fontSize: "1rem",
      letterSpacing: "0.1rem",
    },

    button: {
      fontFamily: "Montserrat",
      fontSize: "0.8rem",
      letterSpacing: "0.1rem",
      textTransform: "capitalize",
    },
  },
  shadows: [
    "0px 0px 0px 0px rgba(0, 120, 0, 0.3)",
    "0px 0px 10px -5px rgba(0, 255, 0, 0.3)",
    "0px 0px 12px -4px rgba(0, 120, 0, 0.3)",
    "0px 2px 14px -3px rgba(0, 120, 0, 0.3)",
    "0px 2px 16px -5px rgba(0, 120, 0, 0.3)",
    "0px 4px 18px -4px rgba(0, 255, 0, 0.3)",
    "0px 4px 20px -3px rgba(0, 120, 0, 0.3)",
    "0px 6px 22px -5px rgba(0, 120, 0, 0.3)",
    "0px 6px 24px -4px rgba(0, 120, 0, 0.3)",
    "0px 8px 26px -3px rgba(0, 120, 0, 0.3)",
    "0px 8px 28px -5px rgba(0, 120, 0, 0.3)",
    "0px 10px 30px -4px rgba(0, 120, 0, 0.3)",
    "0px 10px 32px -3px rgba(0, 120, 0, 0.3)",
  ],
});

export default theme;
