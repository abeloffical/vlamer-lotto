import firebase from "@lib/firebase";

const firestore = firebase.firestore();

export const createUser = async (uid, data) => {
  return firestore
    .collection("users")
    .doc(uid)
    .set({ ...data }, { merge: true });
};

export const getAllUsers = async () => {
  const snapshot = await firestore.collection("users").get();
  let response = {};
  snapshot.forEach((doc) => {
    response[doc.id] = {
      ...doc.data(),
    };
  });
  console.log(response);
  return response;
};
