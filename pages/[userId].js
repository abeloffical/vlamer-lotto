import React from "react";
import { AppBar, Grid, makeStyles, Typography } from "@material-ui/core";

// Components
import AccountPreview from "@components/OwnerAccountPreview";
import { useAuth } from "@lib/auth";
import { getAllUsers } from "@lib/db";

const useStyle = makeStyles((theme) => ({
  container: {
    width: "100vw",
    height: "100vh",
  },
}));

const UserAccount = (props) => {
  const classes = useStyle();

  return (
    <>
      <AccountPreview account={props.user} />
    </>
  );
};

export async function getStaticPaths() {
  const users = await getAllUsers();
  const userIdList = Object.keys(users).map((key) => ({
    params: { userId: key },
  }));

  return {
    paths: [
      ...userIdList, // See the "paths" section below
    ],
    fallback: false, // See the "fallback" section below
  };
}

// This also gets called at build time
export async function getStaticProps({ params }) {
  // params contains the post `id`.
  // If the route is like /posts/1, then params.id is 1

  const users = await getAllUsers();
  const user = users[params.userId];

  // Pass post data to the page via props
  return { props: { user } };
}

export default UserAccount;
