import React from "react";
import { AppBar, Grid, makeStyles } from "@material-ui/core";

// Components
import Layout from "@components/Layout";
import { useAuth } from "@lib/auth";

const useStyle = makeStyles((theme) => ({
  container: {
    width: "100vw",
    height: "100vh",
  },
}));

const Dashboard = () => {
  const classes = useStyle();

  return <Layout />;
};

export default Dashboard;
