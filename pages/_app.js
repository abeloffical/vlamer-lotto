// Core components
import React from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import { Paper, CssBaseline, ThemeProvider } from "@material-ui/core";

// Components
import theme from "@styles/Theme";
import { AuthProvider } from "@lib/auth";
export default function MyApp(props) {
  const { Component, pageProps } = props;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <Paper>
      <Head>
        <meta http='Content-Type' content='text/html; charSet=utf-8' />
        <title>Abel's resume</title>
        <meta
          content='This is personal portfolio site to showcase the projects that abel has been a part of. '
          name='description'
        />
        <meta content='width=device-width, initial-scale=1' name='viewport' />
        <meta http='Content-Type' content='text/html; charSet=utf-8' />
        <meta content='width=device-width, initial-scale=1' name='viewport' />
        <meta name='msapplication-TileColor' content='#2d89ef' />
        <meta name='theme-color' content='#ffffff' />

        <link
          rel='apple-touch-icon'
          sizes='180x180'
          href='/img/favicon/apple-touch-icon.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='32x32'
          href='/img/favicon/favicon-32x32.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='16x16'
          href='/img/favicon/favicon-16x16.png'
        />
        <link rel='manifest' href='/img/favicon/site.webmanifest' />
        <link
          rel='mask-icon'
          href='/img/favicon/safari-pinned-tab.svg'
          color='#5bbad5'
        />
        <meta name='msapplication-TileColor' content='#da532c' />
        <meta name='theme-color' content='#ffffff' />
      </Head>

      <CssBaseline />
      <ThemeProvider theme={theme}>
        <AuthProvider>
          <Component {...pageProps} />
        </AuthProvider>
      </ThemeProvider>
    </Paper>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};
