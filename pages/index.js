import React from "react";
import Head from "next/head";
import { makeStyles } from "@material-ui/core";

// Components
import { useAuth } from "@lib/auth";
import Login from "@components/Login";
import WelcomeScreen from "@components/WelcomeScreen";
const useStyle = makeStyles((theme) => ({
  container: {
    width: "100vw",
    height: "100vh",
  },
}));

const Dashboard = () => {
  const classes = useStyle();
  const auth = useAuth();
  return (
    <>
      <Head>
        <script
          dangerouslySetInnerHTML={{
            __html: `if (document.cookie && document.cookie.includes('vlamer-auth')) {
          window.location.href = "/dashboard"
        }`,
          }}
        />
      </Head>
      {auth?.user ? (
        <div className={classes.container}>
          <WelcomeScreen></WelcomeScreen>
        </div>
      ) : (
        <div className={classes.container}>
          <Login>Hello Vlamers</Login>
        </div>
      )}
    </>
  );
};

export default Dashboard;
