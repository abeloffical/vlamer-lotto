import React from "react";
import { useRouter } from "next/router";
import {
  Box,
  AppBar,
  Toolbar,
  Container,
  makeStyles,
  Tabs,
  Typography,
  Tab,
  Grid,
  Avatar,
} from "@material-ui/core";

import { IoIosSearch } from "react-icons/io";
import { GiGooExplosion } from "react-icons/gi";
import { FiHome, FiSettings } from "react-icons/fi";

// Components
import { useAuth } from "@lib/auth";
import { HideOnScroll } from "@styles/anims/HideOnScroll";
const useStyles = makeStyles((theme) => ({
  appbar: {
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.common.white,
  },
  avatar: {
    boxShadow: theme.shadows[5],
    "&:active": {
      boxShadow: theme.shadows[2],
    },
    "&:hover": {
      boxShadow: theme.shadows[3],
    },
  },
  bottomTab: {
    flexGrow: 1,
    maxWidth: 500,
    position: "fixed",
    bottom: 0,
    left: 0,
    width: "100%",
    backgroundColor: theme.palette.common.white,
  },
  icon: {
    fontSize: "1.4rem",
    color: "black",
  },
  root: {
    // display: "relative",
  },
}));

const Layout = (props) => {
  const classes = useStyles();
  const auth = useAuth();
  const router = useRouter();
  const [value, setValue] = React.useState(0);

  const handlePageChange = (event, newValue) => {
    setValue(newValue);

    console.log(typeof newValue, newValue);
  };

  return (
    <div className={classes.root}>
      <HideOnScroll {...props}>
        <AppBar className={classes.appbar}>
          <Toolbar>
            <Grid container justify='space-between'>
              <Typography variant='h6'>Vlamer</Typography>
              <Avatar
                className={classes.avatar}
                src={auth.user?.imgUrl}
                onClick={() => router.push(`/${auth.user.uid}`)}
              />
            </Grid>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <Toolbar />
      <Container>
        <Box my={2}>
          {[...new Array(6)]
            .map(
              () => `Cras mattis consectetur purus sit amet fermentum.
Cras justo odio, dapibus ac facilisis in, egestas eget quam.
Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
Praesent commodo cursus magna, vel scelerisque nisl consectetur et.`
            )
            .join("\n")}
        </Box>
      </Container>
      <Tabs
        value={value}
        onChange={handlePageChange}
        className={classes.bottomTab}
        variant='fullWidth'
        indicatorColor='secondary'
        textColor='primary'
        aria-label='icon tabs example'
      >
        <Tab icon={<FiHome className={classes.icon} />} aria-label='phone' />
        <Tab
          icon={<GiGooExplosion className={classes.icon} />}
          aria-label='favorite'
        />
        <Tab
          icon={<IoIosSearch className={classes.icon} />}
          aria-label='person'
        />
        <Tab
          icon={<FiSettings className={classes.icon} />}
          aria-label='person'
        />
      </Tabs>
    </div>
  );
};

export default Layout;
