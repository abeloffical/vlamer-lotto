import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

// Icons
import { ImSad } from "react-icons/im";
import { FiSmile } from "react-icons/fi";
import { RiEmotionHappyLine } from "react-icons/ri";

const useStyles = makeStyles((theme) => ({
  tabs: {
    maxWidth: 500,
    paddingTop: theme.spacing(3),
    backgroundColor: "inherit",
  },
  icons: {
    color: "black",
    fontSize: theme.spacing(3.2),
  },
}));

export default function IconTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Tabs
      value={value}
      onChange={handleChange}
      className={classes.tabs}
      variant='fullWidth'
      indicatorColor='primary'
      textColor='primary'
      aria-label='icon tabs example'
    >
      <Tab
        icon={<RiEmotionHappyLine className={classes.icons} />}
        aria-label='phone'
      />
      <Tab icon={<FiSmile />} aria-label='favorite' className={classes.icons} />
      <Tab icon={<ImSad />} aria-label='person' className={classes.icons} />
    </Tabs>
  );
}
