import React from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import {
  Box,
  AppBar,
  Toolbar,
  Container,
  Paper,
  makeStyles,
  Tabs,
  Typography,
  Tab,
  Grid,
  Avatar,
  Button,
} from "@material-ui/core";

import { IoIosSearch } from "react-icons/io";
import { GiGooExplosion } from "react-icons/gi";
import { FiHome, FiSettings } from "react-icons/fi";
import { AiOutlineRollback } from "react-icons/ai";

// Components
import { useAuth } from "@lib/auth";
import { HideOnScroll } from "@styles/anims/HideOnScroll";
import TabsPreview from "@components/AccountPreviewTabs";
const useStyles = makeStyles((theme) => ({
  accountInfoContainer: {
    width: "100%",
    minHeight: "100vh",
  },
  appbar: {
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.common.white,
  },
  avatar: {
    boxShadow: theme.shadows[5],
    width: "5rem",
    height: "5rem",
  },
  accountInfo: {
    padding: theme.spacing(2),
    position: "relative",
    top: "-3.5rem",
  },

  coverImage: {
    width: "100vw",
    height: "10vh",
  },
  icon: {
    fontSize: "1.4rem",
    color: "black",
  },
  root: {
    // display: "relative",
  },

  status: {
    textAlign: "center",
    paddingTop: theme.spacing(3),
  },
  text: {
    paddingTop: theme.spacing(1),
  },
}));

const AccountPreview = ({ account, ...props }) => {
  const classes = useStyles();
  const auth = useAuth();
  const router = useRouter();

  return (
    <div className={classes.root}>
      <HideOnScroll {...props}>
        <AppBar className={classes.appbar}>
          <Toolbar>
            <Grid container justify='space-between'>
              <Grid item>
                <Button
                  variant='contained'
                  className={classes.button}
                  startIcon={<AiOutlineRollback />}
                  onClick={() => router.push("/dashboard")}
                >
                  Back
                </Button>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <Toolbar />
      <div className={classes.accountInfoContainer}>
        <Image
          className={classes.coverImage}
          src='/wlp.jpg'
          width={500}
          height={250}
        />
        <Grid container direction='column' className={classes.accountInfo}>
          <Grid item>
            <Avatar src={account.imgUrl} className={classes.avatar} />
            <Typography variant='h6'> {account.name} </Typography>
            <Typography
              variant='subtitle1'
              color='secondary'
              className={classes.text}
            >
              @{account.username}
            </Typography>
            <Typography variant='body1' className={classes.text}>
              To err is human | Tech enthusiastic👨‍💻 🇪🇹 📍🇸🇪
            </Typography>
          </Grid>
          <Grid item>
            <Grid container justify='space-around'>
              <Typography variant='h6' className={classes.status}>
                0$ <br />
                Revenue
              </Typography>
              <Typography variant='h6' className={classes.status}>
                0$ <br />
                Spent
              </Typography>
              <Typography variant='h6' className={classes.status}>
                0$ <br />
                Volt
              </Typography>
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <Button variant='contained' color='secondary'>
              New
            </Button>
          </Grid>
          <TabsPreview />
        </Grid>
      </div>
    </div>
  );
};

export default AccountPreview;
