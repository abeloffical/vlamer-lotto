import clsx from "classnames";
import {
  Button,
  Divider,
  Grid,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { grey } from "@material-ui/core/colors";

import { FiFacebook, FiTwitter, FiGithub } from "react-icons/fi";
import { FcGoogle } from "react-icons/fc";
import { FaApple } from "react-icons/fa";

// Components
import { useAuth } from "@lib/auth";

const useStyle = makeStyles((theme) => ({
  container: {
    width: "90vw",
    margin: "40% auto",
    padding: "3rem",
    boxShadow: theme.shadows[8],
  },

  button: {
    margin: "1.5rem 0rem",
    padding: `${theme.spacing(2)}px ${theme.spacing(0)}px`,
    letterSpacing: "3px",
    // backgroundColor: "inherit",
  },

  icon: {
    fontSize: theme.spacing(3),
    margin: `0rem ${theme.spacing(1)}px`,
  },

  button_github: {
    backgroundColor: grey[900],
    color: "white",

    "&:active": {
      backgroundColor: grey[800],
      transform: "scale(0.95)",
    },
    "&:hover": {
      backgroundColor: grey[800],
    },
  },

  button_google: {
    backgroundColor: grey[100],
    color: grey[700],
    "&:active": {
      backgroundColor: grey[200],
      transform: "scale(0.95)",
    },
    "&:hover": {
      backgroundColor: grey[200],
    },
  },

  divider: {
    margin: "1rem 0rem",
  },
  more: {
    textAlign: "center",
  },
}));

const Login = (params) => {
  const classes = useStyle();
  const auth = useAuth();

  const handleLogin = (target) => {
    if (target === "github_button") {
      auth.signinWithGithub();
    } else if (target === "google_button") {
      auth.signinWithGoogle();
    } else if (target === "twitter_button") {
      //   console.log("TWITTER");
    } else if (target === "facebook_button") {
      //   console.log("FACEBOOK");
    } else if (target === "apple_button") {
      //   console.log("APPLE");
    }
  };
  return (
    <Grid container direction='column' className={classes.container}>
      <Button
        id='github_button'
        variant='contained'
        className={clsx(classes.button, classes.button_github)}
        onClick={() => handleLogin("github_button")}
      >
        <FiGithub className={classes.icon} />
        Sign in with Github
      </Button>
      <Button
        id='google_button'
        variant='contained'
        className={clsx(classes.button, classes.button_google)}
        onClick={() => handleLogin("google_button")}
      >
        <FcGoogle className={classes.icon} />
        Sign in with Google
      </Button>

      <Divider className={classes.divider} />
      <Typography variant='subtitle2' className={classes.more}>
        Others
      </Typography>
      <Grid container justify='space-around' spacing={2}>
        <IconButton
          id='facebook_button'
          onClick={() => handleLogin("facebook_button")}
        >
          <FiFacebook className={classes.icon} />
        </IconButton>
        <IconButton
          id='twitter_button'
          className={classes.button}
          onClick={() => handleLogin("twitter_button")}
        >
          <FiTwitter className={classes.icon} />
        </IconButton>

        <IconButton
          id='apple_button'
          onClick={() => handleLogin("apple_button")}
        >
          <FaApple className={classes.icon} />
        </IconButton>
      </Grid>
    </Grid>
  );
};

export default Login;
