import clsx from "classnames";
import {
  Avatar,
  Button,
  Divider,
  Grid,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { grey } from "@material-ui/core/colors";

import { FiFacebook, FiTwitter, FiGithub } from "react-icons/fi";
import { FcGoogle } from "react-icons/fc";
import { FaApple } from "react-icons/fa";

import { useAuth } from "@lib/auth";

const useStyle = makeStyles((theme) => ({
  container: {
    width: "90vw",
    margin: "10rem auto",
    padding: theme.spacing(2),
    boxShadow: theme.shadows[8],
    textAlign: "center",
  },

  header: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    boxShadow: theme.shadows[8],
  },
  username: {
    margin: `${theme.spacing(1)}px 0px`,
  },
}));

const welcomeScreen = (params) => {
  const classes = useStyle();
  const auth = useAuth();

  return (
    <Grid
      container
      direction='column'
      alignContent='center'
      spacing={3}
      className={classes.container}
    >
      <Grid item className={classes.header}>
        <Avatar className={classes.avatar} src={auth.user.imgUrl} />
        <Typography className={classes.username} variant='h5' color='primary'>
          {auth.user.name}
        </Typography>
      </Grid>

      <Button variant='outlined' color='primary' onClick={() => auth.signout()}>
        Logout
      </Button>
    </Grid>
  );
};

export default welcomeScreen;
